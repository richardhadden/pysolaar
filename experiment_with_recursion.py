from pysolaar import PySolaar


class First(PySolaar):
    class Meta:
        fields_as_child_docs = [
            "secondInFirst",
            "secondInFirstAgain",
            "secondInFirstMore",
            "secondInFirstEvenmore",
            "thirdInFirst",
        ]

    def build_document_set(self):
        pass

    def build_document(self, identifier):
        return First.Document(
            id=f"first{identifier}",
            secondInFirst=Second.items([f"{identifier}A", f"{identifier}B"]),
            secondInFirstAgain=Second.items([f"{identifier}C", f"{identifier}D"]),
        )


class Second(PySolaar):
    class Meta:
        fields_as_child_docs = ["firstInSecond", "thirdInSecond"]
        # fields_as_json = ["stuff"]

    def build_document_set(self):
        pass

    def build_document(self, identifier):
        return Second.Document(
            id=f"second{identifier}",
            # firstInSecond=First.items(["INNIT"], exclude_fields=["secondInFirst"]),
            firstInSecond=First.items([f"{identifier}£", f"{identifier}$"]),
            thirdInSecond=Third.items(
                "BALLS",
            ),
        )


class Third(PySolaar):
    class Meta:
        fields_as_child_docs = ["firstInThird"]

    def build_document_set(self):
        pass

    def build_document(self, identifier):
        return Third.Document(
            id=f"Third{identifier}",
            third_thing="3thing",
            firstInThird=First.items(
                identifier,
            ),
        )


def thing():
    a = First.build_document(1).doc_to_solr()
    print(a)


thing()
{
    "id": "First#*#*#*#*#first1",
    "_doc": [
        {
            "id": "First#*#*#*#*#Second#*#*#*#*#second1A",
            "_doc": [
                {
                    "id": "Second#*#*#*#*#Third#*#*#*#*#ThirdBALLS",
                    "Second______Third______third_thing": "3thing",
                    "pysolaar_type": "Second#*#*#*#*#Third#*#*#*#*#thirdInSecond",
                }
            ],
            "pysolaar_type": "First#*#*#*#*#Second#*#*#*#*#secondInFirst",
        },
        {
            "id": "First#*#*#*#*#Second#*#*#*#*#second1B",
            "_doc": [
                {
                    "id": "Second#*#*#*#*#Third#*#*#*#*#ThirdBALLS",
                    "Second______Third______third_thing": "3thing",
                    "pysolaar_type": "Second#*#*#*#*#Third#*#*#*#*#thirdInSecond",
                }
            ],
            "pysolaar_type": "First#*#*#*#*#Second#*#*#*#*#secondInFirst",
        },
        {
            "id": "First#*#*#*#*#Second#*#*#*#*#second1C",
            "_doc": [
                {
                    "id": "Second#*#*#*#*#Third#*#*#*#*#ThirdBALLS",
                    "Second______Third______third_thing": "3thing",
                    "pysolaar_type": "Second#*#*#*#*#Third#*#*#*#*#thirdInSecond",
                }
            ],
            "pysolaar_type": "First#*#*#*#*#Second#*#*#*#*#secondInFirstAgain",
        },
        {
            "id": "First#*#*#*#*#Second#*#*#*#*#second1C",
            "_doc": [
                {
                    "id": "Second#*#*#*#*#Third#*#*#*#*#ThirdBALLS",
                    "Second______Third______third_thing": "3thing",
                    "pysolaar_type": "Second#*#*#*#*#Third#*#*#*#*#thirdInSecond",
                }
            ],
            "pysolaar_type": "First#*#*#*#*#Second#*#*#*#*#secondInFirstAgain",
        },
    ],
    "pysolaar_type": "First",
}
