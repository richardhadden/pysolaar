# How the module looks from the outside...
from pysolaar import PySolaar


class Thing(PySolaar):
    class Meta:
        fields_as_json = ["some", "fields", "to", "store"]
        fields_as_child_docs = ["nestyface"]
        input_transformations = {"some": (lambda key, value: (key, value.lower()))}
        output_transformations = {"some": lambda value: value.upper()}

    def build_document_set(self):
        for identifier in [1, 2, 3]:
            yield self.build_document(identifier)

    def build_document(self, identifier):
        return Thing.Document(
            id=f"identifier{identifier}",
            stuff="arse",
            nestyface=AnotherThing.items([1, 2, 3], meta=AnotherThing.NestedMeta),
        )


class AnotherThing(PySolaar):
    def NestedMeta(BaseMeta):
        pass

    def build_document_set(self):
        for identifier in ["list", "of", "identifier"]:
            # This could also be a doc or iterable of docs
            # in which case, yield from it
            yield self.build_document(identifier)

    def build_document(self, identifier):
        # This could return a document or a list of documents
        return AnotherThing.Document(id="identifier", stuff="arse")