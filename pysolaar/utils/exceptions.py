class PySolaarException(Exception):
    pass


class PySolaarConfigurationError(PySolaarException):
    pass


class PySolaarDocumentError(PySolaarException):
    pass