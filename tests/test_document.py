from contextlib import contextmanager
import datetime
import json
from typing import Callable, List, Dict
import pytest

from pysolaar.document import BaseDocument, BaseMeta, _splat_to_list
from pysolaar.utils.exceptions import PySolaarDocumentError
from pysolaar.utils.encoders_and_decoders import ID_SEPARATOR, KEY_SEPARATOR

from .test_pysolaar_config import psc


@pytest.fixture
def doc_cls():
    class Document(BaseDocument):  # n.b. in the actual code these base classes
        class Meta(BaseMeta):  # are automatically injected on init so no worries mate
            pysolaar_type: str = "TestThing"
            fields_as_json: List = []
            fields_as_child_docs: List = []
            input_transformations: Dict[str, Callable] = {}
            output_transformations: Dict[str, Callable] = {}

    yield Document


@pytest.fixture
def meta():
    class Meta(BaseMeta):  # are automatically injected on init so no worries mate
        pysolaar_type: str = (
            "TestThing"  # Put this in here, though should be automatically created
        )
        fields_as_json: List = []
        fields_as_child_docs: List = []
        input_transformations: Dict[str, Callable] = {}
        output_transformations: Dict[str, Callable] = {}

    yield Meta


@pytest.fixture
def child_thing(psc):
    class ChildThing(psc):
        def build_document(self, identifier):
            return [
                ChildThing.Document(
                    id=f"docA-{identifier}",
                    thing="thing_value",
                    thang="thang_value",
                ),
                ChildThing.Document(
                    id=f"docB-{identifier}",
                    thing="thing_value",
                    thang="thang_value",
                ),
            ]

        def build_document_set(self):
            for a in [1, 2, 3]:
                yield self.build_document(a)

    yield ChildThing


@pytest.fixture
def ekv():
    yield BaseDocument._encode_key_values


@pytest.fixture
def doc(doc_cls):
    doc = doc_cls(id="the_id", some_value="the_value")
    yield doc


def test_document_fixture(doc_cls):
    """ sanity checking """
    assert doc_cls.Meta.pysolaar_type == "TestThing"


def test_document_inits_with_fields(doc):
    # doc = document(id="the_id", some_value="the_value")
    assert doc
    assert doc._values == {"id": "the_id", "some_value": "the_value"}


def test_document_values_accessible_as_attrs(doc):
    assert doc
    assert doc.id == "the_id"
    assert doc.some_value == "the_value"


def test_cannot_set_values_on_document_manually(doc):
    with pytest.raises(PySolaarDocumentError):
        doc.id = "something else"


def test_doc_to_solr_adds_pysolaar_type(doc):
    """ Test _encode_key_values includes pysolaar_type """
    assert doc.doc_to_solr()["pysolaar_type"] == "TestThing"


def test_ekv_tackles_id(ekv, meta):
    """ Test _encode_key_values encodes id value correctly """
    assert ekv({"id": "the_id"}, meta)["id"] == f"TestThing{ID_SEPARATOR}the_id"


def test_ekv_encodes_nonspecial(ekv, meta):
    ekv({"something": "somevalue"}, meta)[
        f"TestThing{KEY_SEPARATOR}something"
    ] == "somevalue"


def test_ekv_flattens_dicts(ekv, meta):
    e = ekv({"something": {"field1": "field1value", "field2": ["a", "list"]}}, meta)
    assert e[f"TestThing{KEY_SEPARATOR}something__field1"] == "field1value"
    assert e[f"TestThing{KEY_SEPARATOR}something__field2"] == ["a", "list"]


def test_ekv_flattens_double_nested_dicts(ekv, meta):
    e = ekv(
        {
            "something": {
                "inner_something": {
                    "more_inner_something": {"even_more_inner_something": "emis_value"}
                }
            }
        },
        meta,
    )
    print("E", e)
    assert (
        e[
            f"TestThing{KEY_SEPARATOR}something__inner_something__more_inner_something__even_more_inner_something"
        ]
        == "emis_value"
    )


def test_encode_value_types_with_datetime():
    assert BaseDocument._encode_value_types("a") == "a"
    assert (
        BaseDocument._encode_value_types((datetime.datetime(1988, 1, 1)))
        == "1988-01-01T00:00:00Z"
    )
    assert (
        BaseDocument._encode_value_types(datetime.date(1988, 1, 1))
        == "1988-01-01T00:00:00Z"
    )


def test_ekv_encodes_lists_of_values(ekv, meta):
    e = ekv({"something": ["one", "two", "three"]}, meta)
    assert e[f"TestThing{KEY_SEPARATOR}something"] == ["one", "two", "three"]

    f = ekv(
        {"something": [datetime.datetime(1988, 1, 1), datetime.date(1999, 1, 1)]}, meta
    )
    assert f[f"TestThing{KEY_SEPARATOR}something"] == [
        "1988-01-01T00:00:00Z",
        "1999-01-01T00:00:00Z",
    ]


def test_ekv_throws_error_encoding_lists_of_dicts(ekv, meta):
    with pytest.raises(PySolaarDocumentError):
        e = ekv({"something": [{"a": "thing"}, {"b": "thing"}]}, meta)


def test_ekv_stores_json_fields_as_json(ekv, meta):
    meta.fields_as_json = [
        "something",
    ]

    e = ekv(
        {
            "something": [
                "one",
                "two",
                {
                    "stuffing": "stoffing",
                    "tuffing": ["tuff", "taff", "tiff"],
                    "date": datetime.datetime(1998, 1, 1),
                },
            ],
        },
        meta,
    )

    assert e["something"] == (
        '["one", "two", {"stuffing": "stoffing", "tuffing"'
        ': ["tuff", "taff", "tiff"], "date": "1998-01-01T00:00:00Z"}]'
    )


def test_ekv_stores_dict_fields_as_json(ekv, meta):
    """ Tests we can store nested fields as json by using 'nested' syntax """
    meta.fields_as_json = ["something__inner"]
    e = ekv(
        {
            "something": {
                "inner": ["one", "two", datetime.datetime(1998, 1, 1)],
            },
        },
        meta,
    )
    print("E", e)
    assert (
        e["TestThing______something__inner"] == '["one", "two", "1998-01-01T00:00:00Z"]'
    )
    assert "something" not in e


def test_doc_to_solr_works(doc):
    assert doc.doc_to_solr() == {
        "id": f"TestThing{ID_SEPARATOR}the_id",
        "TestThing______some_value": "the_value",
        "pysolaar_type": "TestThing",
    }


def test_child_doc_doc_to_embedded(child_thing):
    class Meta(BaseMeta):
        pysolaar_type = "ChildThing"

    c_doc = child_thing.Document(id="nested1", stuff="stuffvalue")
    # Have to fabricate a child Meta object with the right pysolaar_type
    c_doc.EmbeddedMeta = Meta
    c = c_doc.doc_to_nested_child("TestThing", "nesty")
    assert c == {
        "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
        "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nested1",
        f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}stuff": "stuffvalue",
    }


@pytest.fixture
def build_mocked_items(mocker):
    """Constructs a PySolaar.items mock method, for a pysolaar type (str)
    and returning a context manager that yields a list of provided docs.

    This fixture is intended for testing Document class by providing a mock
    to be used in ekv `elif key in fields_as_child_docs`."""

    def mocked_items_builder(ps, return_docs):
        class Meta(BaseMeta):
            pysolaar_type = ps

        def mock_items(x, *args, **kwargs):
            @contextmanager
            def context_manager(field_name=None, document_structure=None):
                docs = []
                for d in return_docs:
                    doctype = type(
                        "ps", (BaseDocument,), {}
                    )  # Create a new doctype subclass
                    c = doctype(**d._values)
                    c.Meta = d.Meta
                    c._values["id"] = f"{d.id}-{x}"
                    c.EmbeddedMeta = Meta
                    docs.append(c)
                yield docs

            return context_manager

        gf = mocker.patch("pysolaar.PySolaar.items", mock_items)
        return gf

    return mocked_items_builder


def test_build_mocked_items_fixture(build_mocked_items, doc_cls):
    """Test build_mocked_items fixture with fake classes, and importing
    PySolaar to make sure it's mocked..."""
    from pysolaar.pysolaar import PySolaar

    build_mocked_items("SomeClass", [doc_cls(id="One"), doc_cls(id="Two")])
    value = PySolaar.items(1)
    with value() as values:
        for id, doc in zip(["One-1", "Two-1"], values):
            assert doc.id == id
            assert doc.EmbeddedMeta.pysolaar_type == "SomeClass"


def test_ekv_with_two_nested_documents(ekv, meta, child_thing, build_mocked_items):
    # n.b. can only mock PySolaar.items once!
    build_mocked_items(
        "ChildThing",
        [
            child_thing.Document(id="docA", thing="thing_value", thang="thang_value"),
            child_thing.Document(id="docB", thing="thing_value", thang="thang_value"),
        ],
    )

    meta.fields_as_child_docs = ["nesty", "also_nesty"]
    e = ekv(
        {
            "id": "first_thing",
            "info": "it is big",
            "nesty": child_thing.items(1),
            "also_nesty": child_thing.items(2),
        },
        meta,
    )

    assert e == {
        "id": f"TestThing{ID_SEPARATOR}first_thing",
        f"TestThing{KEY_SEPARATOR}info": "it is big",
        "_doc": [
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docA-1",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thang": "thang_value",
            },
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docB-1",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thang": "thang_value",
            },
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}also_nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docA-2",
                f"TestThing{KEY_SEPARATOR}also_nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}also_nesty{KEY_SEPARATOR}thang": "thang_value",
            },
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}also_nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docB-2",
                f"TestThing{KEY_SEPARATOR}also_nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}also_nesty{KEY_SEPARATOR}thang": "thang_value",
            },
        ],
    }


def test_ekv_with_nested_document_can_also_be_stored_json(
    ekv, meta, child_thing, build_mocked_items
):
    build_mocked_items(
        "ChildThing",
        [
            child_thing.Document(id="docA", thing="thing_value", thang="thang_value"),
            child_thing.Document(id="docB", thing="thing_value", thang="thang_value"),
        ],
    )
    meta.fields_as_child_docs = ["nesty"]
    meta.fields_as_json = ["nesty"]
    e = ekv(
        {
            "id": "first_thing",
            "info": "it is big",
            "nesty": child_thing.items(1),
        },
        meta,
    )

    assert e["id"] == f"TestThing{ID_SEPARATOR}first_thing"
    assert e[f"TestThing{KEY_SEPARATOR}info"] == "it is big"
    # Decode the result, as it's easier than trying to match two
    # separately created json strings
    assert json.loads(e[f"TestThing{KEY_SEPARATOR}nesty"]) == [
        {
            "id": f"docA-1",
            f"thing": "thing_value",
            f"thang": "thang_value",
        },
        {
            "id": f"docB-1",
            f"thing": "thing_value",
            f"thang": "thang_value",
        },
    ]


def test_unpack_meta(meta):
    meta.fields_as_json = ["a_field"]
    assert BaseDocument._unpack_meta("fields_as_json", meta) == set(["a_field"])
    assert BaseDocument._unpack_meta("something_not_there", meta) == set([])


def test_ekv_with_select_fields(ekv, meta):
    meta.select_fields = ["field1"]
    e = ekv({"id": "the_id", "field1": "value1", "field2": "value2"}, meta)
    assert e["id"] == f"TestThing{ID_SEPARATOR}the_id"
    assert e[f"TestThing{KEY_SEPARATOR}field1"] == "value1"
    assert f"TestThing{KEY_SEPARATOR}field2" not in e


def test_ekv_with_exclude_fields(ekv, meta):
    meta.exclude_fields = ["field1"]
    e = ekv({"id": "the_id", "field1": "value1", "field2": "value2"}, meta)
    assert e["id"] == f"TestThing{ID_SEPARATOR}the_id"
    assert e[f"TestThing{KEY_SEPARATOR}field2"] == "value2"
    assert f"TestThing{KEY_SEPARATOR}field1" not in e


def test_doc_to_solr_works_with_nested(doc_cls, child_thing, build_mocked_items):
    build_mocked_items(
        "ChildThing",
        [
            child_thing.Document(id="docA", thing="thing_value", thang="thang_value"),
            child_thing.Document(id="docB", thing="thing_value", thang="thang_value"),
        ],
    )
    doc_cls.Meta.fields_as_child_docs = ["nesty"]
    assert doc_cls(
        id="the_id", some_value="the_value", nesty=child_thing.items(1)
    ).doc_to_solr() == {
        "id": f"TestThing{ID_SEPARATOR}the_id",
        "TestThing______some_value": "the_value",
        "pysolaar_type": "TestThing",
        "_doc": [
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docA-1",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thang": "thang_value",
            },
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docB-1",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thang": "thang_value",
            },
        ],
    }


def test_real_example(ekv, meta):
    """When building APIS data, some of the fields with this nested are missing. Trying to find out why.
    It's not here!"""
    assert ekv(
        {
            "id": "27527_PersonInstitution_96054",
            "name": "",
            "statementType": {
                "url": "statement_type_url",
                "label": "statement_type_label",
            },
            "memberOf": {
                "uri": [
                    "http://d-nb.info/gnd/1700400-7",
                    "/apis/entities/entity/institution/96052/detail",
                ],
                "label": "Kaiserlich-Königliches Lyceum (Graz)",
            },
            "place": {},
            "relatesToPerson": {},
            "role": {"url": "NONE", "label": "war Student"},
            "date": {
                "sortdate": datetime.date(1820, 1, 1),
                "label": "1820-01-01-1822-01-01",
            },
            "statementText": "Tangl, Karlmann (war Student) Kaiserlich-Königliches Lyceum (Graz)",
        },
        meta,
    ) == {
        "id": f"TestThing{ID_SEPARATOR}27527_PersonInstitution_96054",
        "TestThing______name": "",
        "TestThing______statementType__url": "statement_type_url",
        "TestThing______statementType__label": "statement_type_label",
        "TestThing______memberOf__uri": [
            "http://d-nb.info/gnd/1700400-7",
            "/apis/entities/entity/institution/96052/detail",
        ],
        "TestThing______memberOf__label": "Kaiserlich-Königliches Lyceum (Graz)",
        "TestThing______role__url": "NONE",
        "TestThing______role__label": "war Student",
        "TestThing______date__sortdate": "1820-01-01T00:00:00Z",
        "TestThing______date__label": "1820-01-01-1822-01-01",
        "TestThing______statementText": "Tangl, Karlmann (war Student) Kaiserlich-Königliches Lyceum (Graz)",
    }


def test_ekv_with_nested_documents_change_field_names(
    ekv, meta, child_thing, build_mocked_items
):
    """ replaces test_ekv_with_nested_document """
    build_mocked_items(
        "ChildThing",
        [
            child_thing.Document(id="docA", thing="thing_value", thang="thang_value"),
            child_thing.Document(id="docB", thing="thing_value", thang="thang_value"),
        ],
    )
    meta.fields_as_child_docs = ["nesty"]
    e = ekv(
        {
            "id": "first_thing",
            "info": "it is big",
            "nesty": child_thing.items(1),  # build_mocked_items above replaces this
        },
        meta,
    )

    assert e == {
        "id": f"TestThing{ID_SEPARATOR}first_thing",
        f"TestThing{KEY_SEPARATOR}info": "it is big",
        "_doc": [
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docA-1",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thang": "thang_value",
            },
            {
                "pysolaar_type_nested": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}nesty",
                "id": f"TestThing{ID_SEPARATOR}ChildThing{ID_SEPARATOR}docB-1",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thing": "thing_value",
                f"TestThing{KEY_SEPARATOR}nesty{KEY_SEPARATOR}thang": "thang_value",
            },
        ],
    }


"""
# Can't really test this like this, as it's a context manager which requires unpacking
# but from test_pysolaar_config::test_store_document_fields_with_SplattedChildDocument
# can see that it works

def test_ekv_with_doc_as_splatted_list(ekv, meta, child_thing, build_mocked_items):

    build_mocked_items(
        "ChildThing",
        [
            child_thing.Document(id="docA", thing="thing_value", thang="thang_value"),
            child_thing.Document(id="docB", thing="thing_value", thang="thang_value"),
        ],
    )
    meta.fields_as_splatted_child_docs = ["nesty"]

    e = ekv(
        {
            "id": "first_thing",
            "info": "it is big",
            "nesty": child_thing.items(1),  # build_mocked_items above replaces this
        },
        meta,
    )

    assert e["id"] == f"TestThing{ID_SEPARATOR}first_thing"
    assert e[f"TestThing{KEY_SEPARATOR}info"] == "it is big"
    assert e[f"TestThing{KEY_SEPARATOR}nesty"] == [
        "docA-1",
        "thing_value",
        "thang_value",
        "docB-1",
        "thing_value",
        "thang_value",
    ]
"""


def test_splat_to_list():
    assert _splat_to_list({1: "a", 2: "b"}) == ["a", "b"]

    assert _splat_to_list({1: ["a", "a1"], 2: "b"}) == ["a", "a1", "b"]

    assert _splat_to_list({1: {3: "a"}, 2: "b"}) == ["a", "b"]