from pysolaar.utils.meta_utils import SingleValue, TransformKey, TransformValues
from pysolaar import PySolaar, AsDict, DocumentFields, ChildDocument, Q

import pysolr

import pytest

PYSOLR_URL = "http://localhost:8983/solr/test_solr"


@pytest.fixture(scope="module")
def solr():
    client = pysolr.Solr(PYSOLR_URL, always_commit=True)
    client.delete(q="*:*")
    yield client
    client.delete(q="*:*")  # Reset Solr by trashing everything


@pytest.fixture
def cpsc(solr):
    """ Pre-configured PySolaar class """
    PySolaar._RESET()  # Reset subclasses
    solr.delete(q="*:*")
    PySolaar.configure_pysolr(pysolr_object=solr)
    yield PySolaar
    PySolaar._RESET()  # Reset subclasses


NUMBERS = ["zero", "one", "two", "three"]
WORDS = ["buzzard", "cantaloupe", "alsatian", "meringue"]
CHILD_WORDS = ["pebble", "rock", "stone", "boulder"]


def test_generic_data_and_querying(cpsc):
    class Thing(cpsc):
        class Meta:
            store_document_fields = DocumentFields(
                number=True,
                word=True,
                children=ChildDocument(
                    child_word=True,
                ),
            )
            return_document_fields = DocumentFields(
                id=TransformKey("@id"),
                number=SingleValue,
                word=SingleValue,
                children=ChildDocument(
                    child_word=TransformValues(lambda x: x.upper()) & SingleValue,
                ),
            )

        def build_document_set(self):
            for i in [0, 1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=f"{identifier}",
                number=NUMBERS[identifier],
                word=WORDS[identifier],
                children=ChildThing.items(identifier),
            )

    class ChildThing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id=f"c{identifier}",
                child_word=CHILD_WORDS[identifier],
            )

    cpsc.update()

    a = (
        Thing.filter(Q(word="buzzard") | Q(word="cantaloupe"))
        .filter_by_distinct_child(field_name="children", child_word="pebble")
        .all()
    )
    for item in a:
        assert item == {
            "@id": "0",
            "number": "zero",
            "word": "buzzard",
            "children": [
                {
                    "child_word": "PEBBLE",
                }
            ],
        }

    assert len(Thing) == 4

    b = Thing.filter(Q(word="buzzard") | Q(word="cantaloupe"))
    assert len(b) == 2
    c = b.all()
    assert len(c) == 2
    assert b.first() == {
        "@id": "0",
        "number": "zero",
        "word": "buzzard",
        "children": [
            {
                "child_word": "PEBBLE",
            }
        ],
    }

    assert Thing.first() == {
        "@id": "0",
        "number": "zero",
        "word": "buzzard",
        "children": [
            {
                "child_word": "PEBBLE",
            }
        ],
    }
    assert Thing.paginate(page_number=1, page_size=1).first() == {
        "@id": "1",
        "children": [
            {
                "child_word": "ROCK",
            }
        ],
        "number": "one",
        "word": "cantaloupe",
    }


def test_ordering(cpsc):
    class Thing(cpsc):
        def build_document(self, identifier):
            return self.Document(id=identifier, thing=["one", "two"][identifier-1])
        
        def build_document_set(self):
            yield from (self.build_document(n) for n in [1, 2])

    cpsc.update()

    assert Thing.order_by("id desc").first()["id"] == "2"