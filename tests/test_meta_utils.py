import datetime
import json


import pytest

from pysolaar.utils.exceptions import PySolaarConfigurationError
from pysolaar.utils.meta_utils import (
    AsDict,
    AsDateTime,
    BaseMeta,
    ChildDocument,
    DocumentFields,
    JsonChildDocument,
    JsonToDict,
    SingleValue,
    Transform,
    TransformKey,
    TransformValues,
    _fields_from_meta_return_document_fields,
)


def test_single_value():
    assert SingleValue().transform_function("key", ["thing"]) == ("key", "thing")
    assert SingleValue.transform_function("key", ["thing"]) == ("key", "thing")
    assert SingleValue.transform_function("key", "thing") == ("key", "thing")

    # Test that with some metaclass modification we can do this __and__ join
    # to the class itself
    a = SingleValue & TransformKey("KEY")
    assert a.transform_function("key", ["thing"]) == ("KEY", "thing")


def test_json_to_dict():
    assert JsonToDict.transform_function("key", [json.dumps({"hi": "there"})],) == (
        "key",
        {"hi": "there"},
    )

    assert JsonToDict(hi=TransformKey("HI")).transform_function(
        "key",
        [json.dumps({"hi": "there"})],
    ) == (
        "key",
        {"HI": "there"},
    )

    # Test that with some metaclass modification we can do this __and__ join
    # to the class itself
    a = JsonToDict & SingleValue
    assert a.transform_function("key", [json.dumps([{"hi": "there"}])],) == (
        "key",
        {"hi": "there"},
    )

    a = JsonToDict(hi=TransformKey("HI")) & SingleValue
    assert a.transform_function("key", [json.dumps([{"hi": "there"}])],) == (
        "key",
        {"HI": "there"},
    )


def test_as_datetime_transform():
    assert AsDateTime.transform_function("key", ["1998-01-01T00:00:00Z"]) == (
        "key",
        datetime.datetime(1998, 1, 1),
    )


def test_child_document_can_have_transforms():
    c = ChildDocument(something=True) & SingleValue
    assert c["something"] == True

    assert c.transform_function is SingleValue.transform_function

    assert c.transform_function("key", ["value"]) == ("key", "value")

    d = c & TransformKey("@KEY")

    assert d.transform_function("key", ["value"]) == ("@KEY", "value")


def test_transforms():
    t = Transform(lambda key, value: (key.upper(), [v.upper() for v in value]))
    assert t.transform_function("key", ["value"]) == ("KEY", ["VALUE"])

    t = TransformKey(lambda key: key.upper())
    assert t.transform_function("key", ["value"]) == ("KEY", ["value"])

    t = TransformKey("@id")
    assert t.transform_function("key", ["value"]) == ("@id", ["value"])

    t = TransformValues(lambda value: value.upper())
    assert t.transform_function("key", ["value"]) == ("key", ["VALUE"])


def test_transform_combining():
    t = TransformValues(lambda value: value.upper()) & SingleValue
    assert t.transform_function("key", ["value"]) == ("key", "VALUE")

    with pytest.raises(PySolaarConfigurationError):
        t = TransformValues(lambda value: value.upper()) & "some shit"


def test_fields_from_meta_return_document_fields():
    class Meta(BaseMeta):
        return_document_fields = DocumentFields(
            id=TransformKey(lambda k: "@id"),
            something=SingleValue,
            a_dict=AsDict(
                dict1=SingleValue,
                dict2=AsDateTime,
                dict3=AsDict(
                    d3inner1=True,
                    d3inner2=True,
                ),
            ),
        )

    t = _fields_from_meta_return_document_fields(Meta)
    assert "something" in t
    assert "a_dict__dict1" in t
    assert "a_dict__dict2" in t
    assert "a_dict__dict3__d3inner1" in t
    assert "a_dict__dict3__d3inner2" in t
