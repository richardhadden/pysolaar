import datetime
import time

import pysolr
import pytest

from pysolaar.pysolaar import PySolaar, DocumentFields, ChildDocument
from pysolaar.utils.meta_utils import JsonChildDocument
from pysolaar.utils.encoders_and_decoders import ID_SEPARATOR, KEY_SEPARATOR

from .test_pysolaar_config import psc

PYSOLR_URL = "http://localhost:8983/solr/test_solr"


# Here we configure a PySolr client so we can access Solr to check
# PySolaar is working as expected.
# Also make sure we clean up properly after each test!
@pytest.fixture(scope="module")
def solr():

    client = pysolr.Solr(PYSOLR_URL, always_commit=True)

    client.delete(q="*:*")
    print(client.search("*:*"))
    yield client
    client.delete(q="*:*")  # Reset Solr by trashing everything


def test_solr(solr):
    """Test for my sanity between tests, that we can add
    docs to Solr and trash them all again
    """
    solr.add(
        [{"id": "thing", "text": "thingy"}, {"id": "otherthing", "text": "otherthingy"}]
    )
    result = solr.search(q="*:*")
    assert len(result) == 2
    solr.delete(q="*:*")
    result = solr.search(q="*:*")
    assert len(result) == 0


@pytest.fixture
def cpsc(psc, solr):
    """ Pre-configured PySolaar class """
    solr.delete(q="*:*")
    psc.configure_pysolr(pysolr_object=solr)
    yield psc


def test_update_with_single_class(cpsc, solr):
    WORDS = {1: "something", 2: "other", 3: "whatnot"}

    class Thing(cpsc):
        def build_document_set(self):
            for i in [1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=f"thing{identifier}",
                value=WORDS[identifier],
            )

    cpsc.update()

    result = solr.search(q="*:*")
    assert len(result) == 3
    for i, result in enumerate(result, 1):
        assert result["id"] == f"Thing{ID_SEPARATOR}thing{i}"
        # Values from Solr are by default lists
        assert result[f"Thing{KEY_SEPARATOR}value"] == [WORDS[i]]


def test_update_clears_cache(cpsc, solr):
    WORDS = {1: "something", 2: "other", 3: "whatnot"}

    class Thing(cpsc):
        def build_document_set(self):
            for i in [1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=f"thing{identifier}",
                value=WORDS[identifier],
            )

    cpsc.update()
    assert Thing._DOCUMENT_CACHE == {}

def test_update_with_nested_document_works(cpsc, solr):
    WORDS = {1: "something", 2: "other", 3: "whatnot"}

    class Thing(cpsc):
        class Meta:
            fields_as_child_docs = ["nested"]

        def build_document_set(self):
            for i in [1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=f"thing{identifier}",
                value=WORDS[identifier],
                nested=NestedThing.items(identifier),
            )

    class NestedThing(cpsc):
        build_document_set = None

        def build_document(self, identifier):

            return self.Document(
                id=f"nestedthing{identifier}", value=f"nested {WORDS[identifier]}"
            )

    cpsc.update()

    results = solr.search(q="*:*", fl="*,[child]")

    # Iterate the results in numbered groups of three:
    # two versions of nested from pre-push and push (if there was a way to tackle this?)
    # and the actual doc
    for i, (other_nested, nested, doc) in enumerate(zip(*[iter(results)] * 3), start=1):
        assert (
            nested["id"]
            == f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nestedthing{i}"
        )
        assert nested[f"Thing{KEY_SEPARATOR}nested{KEY_SEPARATOR}value"] == [
            f"nested {WORDS[i]}"
        ]
        assert nested["pysolaar_type_nested"] == [
            f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nested"
        ]
        assert doc["id"] == f"Thing{ID_SEPARATOR}thing{i}"
        assert doc[f"Thing{KEY_SEPARATOR}value"] == [WORDS[i]]
        assert doc["_doc"] == nested


def test_chunk_size(cpsc, mocker):
    WORDS = {1: "something", 2: "other", 3: "whatnot"}

    # Replace the pysolr.Solr add method as we don't want to actually push
    # anything to Solr at the moment
    mock_add = mocker.patch("pysolr.Solr.add")

    class Thing(cpsc):
        class Meta:
            fields_as_child_docs = ["nested"]

        def build_document_set(self):
            for i in [1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=f"thing{identifier}",
                value=WORDS[identifier],
            )

    # Call update() with no chunk size, expect it to push
    # everything at once
    cpsc.update()
    mock_add.assert_called_once_with(
        [
            {
                "Thing______value": "something",
                "id": f"Thing{ID_SEPARATOR}thing1",
                "pysolaar_type": "Thing",
            },
            {
                "Thing______value": "other",
                "id": f"Thing{ID_SEPARATOR}thing2",
                "pysolaar_type": "Thing",
            },
            {
                "Thing______value": "whatnot",
                "id": f"Thing{ID_SEPARATOR}thing3",
                "pysolaar_type": "Thing",
            },
        ],
    )

    mock_add.reset_mock()

    # Now, with chunk size set to 1, it should push each document
    # as a separate call, in order
    cpsc.update(max_chunk_size=1)
    mock_add.assert_has_calls(
        [
            mocker.call(
                [
                    {
                        "Thing______value": "something",
                        "id": f"Thing{ID_SEPARATOR}thing1",
                        "pysolaar_type": "Thing",
                    },
                ],
            ),
            mocker.call(
                [
                    {
                        "Thing______value": "other",
                        "id": f"Thing{ID_SEPARATOR}thing2",
                        "pysolaar_type": "Thing",
                    },
                ],
            ),
            mocker.call(
                [
                    {
                        "Thing______value": "whatnot",
                        "id": f"Thing{ID_SEPARATOR}thing3",
                        "pysolaar_type": "Thing",
                    },
                ],
            ),
        ]
    )


def test_chunks_work_for_sub_doc(cpsc, mocker):
    WORDS = {1: "something", 2: "other", 3: "whatnot"}
    mock_add = mocker.patch("pysolr.Solr.add")

    class Thing(cpsc):
        class Meta:
            fields_as_child_docs = ["nested"]

        def build_document_set(self):
            for i in [1]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=f"thing{identifier}",
                value=WORDS[identifier],
                nested=NestedThing.items(identifier),
            )

    class NestedThing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id=f"nestedthing{1}", value=f"nested {WORDS[identifier]}"
            )

    cpsc.update(max_chunk_size=1)
    print(mock_add.mock_calls)
    mock_add.assert_has_calls(
        [
            mocker.call(
                [
                    {
                        f"Thing{KEY_SEPARATOR}nested{KEY_SEPARATOR}value": f"nested {WORDS[1]}",
                        "id": f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nestedthing1",
                        "pysolaar_type_nested": f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nested",
                    },
                ],
            ),
            mocker.call(
                [
                    {
                        f"Thing{KEY_SEPARATOR}value": WORDS[1],
                        "id": f"Thing{ID_SEPARATOR}thing1",
                        "pysolaar_type": "Thing",
                        "_doc": [
                            {
                                f"Thing{KEY_SEPARATOR}nested{KEY_SEPARATOR}value": f"nested {WORDS[1]}",
                                "id": f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nestedthing1",
                                "pysolaar_type_nested": f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nested",
                            }
                        ],
                    },
                ],
            ),
        ]
    )


def test_pushing_real_example(cpsc, solr):

    """Using data from test_pysolaar_config real example, check we can actually push it to solr"""

    class FactoidIndex(cpsc):
        class Meta:
            store_document_fields = DocumentFields(
                createdBy=True,
                createdWhen=True,
                modifiedBy=True,
                modifiedWhen=True,
                statement_refs=JsonChildDocument(
                    id=True,
                ),
                statements=ChildDocument(
                    id=True,
                    name=True,
                    statementType=True,
                    memberOf=True,
                    place=True,
                    relatedToPerson=True,
                    role=True,
                    date=True,
                    statementText=True,
                ),
            )

        build_document_set = None

        def build_document(self, identifier):
            # instance, source = identifier
            instance = identifier
            res = {}

            res["createdBy"] = "Smith"
            res["createdWhen"] = datetime.date(1999, 1, 1)
            res["modifiedBy"] = "Jones"
            res["modifiedWhen"] = datetime.date(1999, 1, 2)

            res["statements"] = StatementIndex.items(["A"])

            return FactoidIndex.Document(id=f"factoid_{'A'}", **res)

    class StatementIndex(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            res = {
                "@id": "27527_PersonInstitution_96054",
                "name": "",
                "statementType": {
                    "url": "statement_type_url",
                    "label": "statement_type_label",
                },
                "memberOf": {
                    "uri": [
                        "http://d-nb.info/gnd/1700400-7",
                        "/apis/entities/entity/institution/96052/detail",
                    ],
                    "label": "Kaiserlich-Königliches Lyceum (Graz)",
                },
                "place": {},
                "relatesToPerson": {},
                "role": {"url": "NONE", "label": "war Student"},
                "date": {
                    "sortdate": datetime.date(1820, 1, 1),
                    "label": "1820-01-01-1822-01-01",
                },
                "statementText": "Tangl, Karlmann (war Student) Kaiserlich-Königliches Lyceum (Graz)",
            }
            yield self.Document(id=res.pop("@id"), **res)

            res = {
                "@id": "27527_PersonPlace_141779",
                "name": "",
                "statementType": {
                    "url": "statement_type_url",
                    "label": "statement_type_label",
                },
                "memberOf": {},
                "place": {
                    "uri": [
                        "http://sws.geonames.org/2760910/",
                        "/apis/entities/entity/place/21140/detail",
                    ],
                    "label": "Wolfsberg",
                },
                "relatesToPerson": {},
                "role": {"url": "NONE", "label": "ausgebildet in"},
                "date": {"sortdate": None, "label": "None"},
                "statementText": "Tangl, Karlmann (ausgebildet in) Wolfsberg",
            }
            yield self.Document(id=res.pop("@id"), **res)

    cpsc.update()

    results = solr.search(q="*:*", fl="*,[child]")
    # assert len(results) == 9 # Can't make this work and have the test fixture scrubbing
    # properly


def test_we_can_do_simple_query(cpsc):
    class Thing(cpsc):
        def build_document_set(self):
            for doc in [self.build_document(1), self.build_document(2)]:
                yield from doc

        def build_document(self, identifier):
            ### TODO: SOMETHING (itertools tee craps out if you do this
            # but that's ok, we weren't going to do that... REFACTOR to cache
            # results, don't spread tee everywhere)
            if identifier == 1:
                yield self.Document(id=1, value="one")
            elif identifier == 2:
                yield self.Document(id=2, value="two")

    cpsc.update()

    result = Thing.filter(value="one")
    assert len(result) == 1
    # Don't panic if this fails! We'll need to update the result objects!
    for r in result:
        assert r["id"] == f"1"

    result = Thing.filter(value="two")
    assert len(result) == 1
    # Don't panic if this fails! We'll need to update the result objects!
    for r in result:
        assert r["id"] == f"2"


def test_len_works_in_real_world(cpsc):
    class Thing(cpsc):
        def build_document_set(self):
            for doc in [self.build_document(1), self.build_document(2)]:
                yield from doc

        def build_document(self, identifier):
            ### TODO: SOMETHING (itertools tee craps out if you do this
            # but that's ok, we weren't going to do that... REFACTOR to cache
            # results, don't spread tee everywhere)
            if identifier == 1:
                yield self.Document(id=1, value="one")
            elif identifier == 2:
                yield self.Document(id=2, value="two")

    cpsc.update()

    assert len(Thing) == 2
    for i, item in enumerate(Thing, start=1):
        assert item["id"] == f"{i}"


def test_query_with_nested_doc(cpsc):
    WORDS = {1: "something", 2: "other", 3: "whatnot", 4: "bull", 5: "shit"}

    class NewThing(cpsc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                child=ChildDocument(
                    value=True,
                    other=True,
                ),
                fuck=ChildDocument(
                    value=True,
                    other=True,
                ),
            )

        def build_document_set(self):
            for i in [1]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=identifier,
                value="NOTHING",
                child=ChildThing.items(identifier),
                fuck=ChildThing.items(identifier + 1),
            )

    class ChildThing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id=f"C{identifier}",
                value=WORDS[identifier],
                other=WORDS[identifier + 1],
            )

    cpsc.update()

    assert len(NewThing.all()) == 1

    a = NewThing.filter_by_distinct_child(
        field_name="child",
        value="something",
    )
    assert len(a) == 1

    a = NewThing.filter_by_distinct_child(
        field_name="child",
        value="something",
    ).filter_by_distinct_child(
        field_name="child",
        value="IMPOSSIBLE",
    )
    assert len(a) == 0

    a = NewThing.filter_by_distinct_child(
        field_name="child", value="something", other="other"
    )
    assert len(a) == 1

    a = NewThing.filter_by_distinct_child(
        field_name="child", value="something", other="something"
    )
    assert len(a) == 0


def test_query_with_nested_doc2(cpsc):
    WORDS = {1: "something", 2: "other", 3: "whatnot", 4: "bull", 5: "shit"}

    class NewThing(cpsc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                child=ChildDocument(
                    value=True,
                ),
            )

        def build_document_set(self):
            for i in [1]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=identifier,
                value="NOTHING",
                child=ChildThing.items([1, 2]),
            )

    class ChildThing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id=f"C{identifier}",
                value=WORDS[identifier],
            )

    cpsc.update()

    # These two queries should work separately, referring as they do to two separate
    # child docs with different values
    a = NewThing.filter_by_distinct_child(field_name="child", value="something")
    assert len(a) == 1

    a = NewThing.filter_by_distinct_child(field_name="child", value="other")
    assert len(a) == 1

    # This should also work, because we're saying, find me the parent that has
    # both these children
    a = NewThing.filter_by_distinct_child(
        field_name="child", value="something"
    ).filter_by_distinct_child(field_name="child", value="other")
    assert len(a) == 1


def test_query_with_nested_doc3(cpsc):
    WORDS = {1: "something", 2: "other", 3: "whatnot", 4: "bull", 5: "shit"}

    class NewThing(cpsc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                child=ChildDocument(
                    value=True,
                ),
            )

        def build_document_set(self):
            for i in [1, 2]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return self.Document(
                id=identifier,
                value="NOTHING",
                child=ChildThing.items([identifier, identifier + 1]),
            )

    class ChildThing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id=f"C{identifier}",
                value=WORDS[identifier],
            )

    cpsc.update()

    a = NewThing.filter_by_distinct_child(field_name="child", value="other")
    assert len(a) == 2

    # Add another filter to distinguish between the two
    b = a.filter_by_distinct_child(field_name="child", value="something")
    assert len(b) == 1

    # HERE is the data queried in this test...
    [
        {
            "id": "NewThing##########1",
            "NewThing______value": "NOTHING",
            "_doc": [
                {
                    "id": "NewThing##########ChildThing##########C1",
                    "NewThing______child______value": "something",
                    "pysolaar_type_nested": "NewThing##########ChildThing##########child",
                },
                {
                    "id": "NewThing##########ChildThing##########C2",
                    "NewThing______child______value": "other",
                    "pysolaar_type_nested": "NewThing##########ChildThing##########child",
                },
            ],
            "pysolaar_type": "NewThing",
        },
        {
            "id": "NewThing##########2",
            "NewThing______value": "NOTHING",
            "_doc": [
                {
                    "id": "NewThing##########ChildThing##########C2",
                    "NewThing______child______value": "other",
                    "pysolaar_type_nested": "NewThing##########ChildThing##########child",
                },
                {
                    "id": "NewThing##########ChildThing##########C3",
                    "NewThing______child______value": "whatnot",
                    "pysolaar_type_nested": "NewThing##########ChildThing##########child",
                },
            ],
            "pysolaar_type": "NewThing",
        },
    ]


def test_querying_dates(cpsc):
    class Stuff(cpsc):
        def build_document_set(self):
            yield from self.build_document(1)

        def build_document(self, identifier):
            return [
                self.Document(
                    id=1, stuff="something", date=datetime.datetime(1990, 1, 1)
                ),
                self.Document(id=2, stuff="other", date=datetime.datetime(1992, 1, 1)),
            ]

    cpsc.update()

    assert len(Stuff) == 2

    a = Stuff.filter(stuff="something")
    assert len(a) == 1

    b = Stuff.filter(date__lt=datetime.date(1991, 1, 1))
    assert len(b) == 1

    c = Stuff.filter(date__gt=datetime.date(1991, 1, 1))
    assert len(c) == 1

    d = Stuff.filter(date__gt=datetime.date(1989, 1, 1))
    assert len(d) == 2

    e = Stuff.filter(date__gte=datetime.date(1990, 1, 1))
    assert len(e) == 2


def test_getting_some_results(cpsc):
    class Stuff(cpsc):
        def build_document_set(self):
            yield from self.build_document(1)

        def build_document(self, identifier):
            return [
                self.Document(
                    id=1, stuff="something", date=datetime.datetime(1990, 1, 1)
                ),
                self.Document(id=2, stuff="other", date=datetime.datetime(1992, 1, 1)),
            ]

    cpsc.update()

    assert len(Stuff)

    # assert 2 == 1

    [
        {
            "id": "Stuff##########1",
            "Stuff______stuff": "something",
            "Stuff______date": "1990-01-01T00:00:00Z",
            "pysolaar_type": "Stuff",
        },
        {
            "id": "Stuff##########2",
            "Stuff______stuff": "other",
            "Stuff______date": "1992-01-01T00:00:00Z",
            "pysolaar_type": "Stuff",
        },
    ]

    [
        {
            "id": "Stuff##########1",
            "Stuff______stuff": ["something"],
            "Stuff______date": ["1990-01-01T00:00:00Z"],
            "pysolaar_type": ["Stuff"],
            "_version_": 1694926616599199744,
        },
        {
            "id": "Stuff##########2",
            "Stuff______stuff": ["other"],
            "Stuff______date": ["1992-01-01T00:00:00Z"],
            "pysolaar_type": ["Stuff"],
            "_version_": 1694926616602345472,
        },
    ]
