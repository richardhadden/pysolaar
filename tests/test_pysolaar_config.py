import datetime
import inspect
import itertools
import json
from unittest import mock

from typing import Generator, Type
import pytest
import pysolr

from pysolaar import __version__

from pysolaar.pysolaar import CachedGenerator, PySolaar, PySolaarConfigurationWarning
from pysolaar.queryset import PySolaarQuerySetBase, Q
from pysolaar.document import (
    BaseDocument,
)
from pysolaar.utils.exceptions import PySolaarConfigurationError, PySolaarDocumentError
from pysolaar.utils.encoders_and_decoders import ID_SEPARATOR, KEY_SEPARATOR
from pysolaar.utils.meta_utils import (
    BaseMeta,
    ChildDocument,
    DocumentFields,
    JsonChildDocument,
    SplattedChildDocument,
)


@pytest.fixture
def psc() -> Generator[Type[PySolaar], Type[PySolaar], None]:
    """ Our PySolaar class, with subclasses removed between each test """
    PySolaar._RESET()  # Reset subclasses
    yield PySolaar
    PySolaar._RESET()  # Reset subclasses


@pytest.fixture()
def thing_object(psc):
    """ A basic PySolaar object for testing """

    class Thing(psc):
        def build_document(self, identifier):
            return Thing.Document(
                id=f"identifier{identifier}", thing="thing_value", thang="thang_value"
            )

        def build_document_set(self):
            for a in [1, 2, 3]:
                yield self.build_document(a)

    yield Thing


def test_version():
    assert __version__ == "0.1.0"


def test_import_pysolaar():
    assert PySolaar.Document


def test_pysolaar_class_fixture(psc):
    assert psc.Document


def test_basic_pysolaar_object_fixture(psc, thing_object):
    assert thing_object.Document


def test_subclass_not_implementing_build_document_raises_error(psc):
    with pytest.raises(PySolaarConfigurationError):

        class MissingBuildDocument(psc):
            def build_document_set(self):
                pass


@pytest.mark.filterwarnings("ignore::UserWarning")
def test_subclass_not_implementing_is_ok_actually(psc):
    """Previous behaviour was to require build_document_set for each class, but
    this may not be required;

    with pytest.raises(PySolaarConfigurationError):

        class MissingBuildDocument(psc):
            def build_document(self):
                pass

    """

    with pytest.warns(PySolaarConfigurationWarning):

        class MissingBuildDocumentSet(psc):
            def build_document(self):
                pass

    assert True


def test_subclass_names_are_unique(psc):
    class SomeClass(psc):
        def build_document(self):
            pass

        def build_document_set(self):
            pass

    with pytest.raises(PySolaarConfigurationError):

        class SomeClass(psc):
            def build_document(self):
                pass

            def build_document_set(self):
                pass


def test_initialised_document_class(psc, thing_object):
    assert thing_object.Document
    assert thing_object.Document.__name__ == "ThingDocument"
    assert thing_object.Document.Meta == thing_object.Meta
    assert thing_object.Document.Meta.pysolaar_type == "Thing"
    assert thing_object.Document(id=1, something="what")


def test_overriding_document_class():
    """If you want to do some really nutjob shit and declare your own document classes,
    you can."""

    class NewTestObject(PySolaar):
        class Document:
            is_overridden = "YES IT IS"

        def build_document_set(self):
            pass

        def build_document(self):
            pass

    assert NewTestObject.Document.is_overridden == "YES IT IS"
    assert NewTestObject.Document.doc_to_solr


@pytest.fixture
def patch_calling_class_name(psc, mocker):
    """The PySolaar.items method uses sys._getframe(n).f_locals["self"].__name__
    in order to get the class in which the call to this method is embedded
    ('calling_class_name'). In order to test, we need to simulate this by
    patching this whole operation.
    """

    def patcher(name_to_return):
        def mock_getframe(*args, **kwargs):
            t = type(
                name_to_return,
                (object,),
                {},
            )

            class Frame:
                f_locals = {"self": t}

            return Frame

        patched_sys_getframe = mocker.patch("sys._getframe", mock_getframe)
        return patched_sys_getframe

    return patcher


def test_patch_calling_class_name(patch_calling_class_name):
    patch_calling_class_name("TestType")

    import sys

    assert sys._getframe(1).f_locals["self"].__name__ == "TestType"


def test_entity_items_method_returns_list_of_docs(
    thing_object, patch_calling_class_name
):
    patch_calling_class_name("Thing")
    toi = thing_object.items([1, 2, 3])

    with toi() as values:
        for id, doc in enumerate(values, 1):
            assert doc.id == f"identifier{id}"
            assert doc.Meta.is_embedded == True
            assert doc.EmbeddedMeta.is_embedded == True


def test_entity_items_method_takes_single_identifier(
    thing_object, patch_calling_class_name
):
    patch_calling_class_name("Thing")
    toi = thing_object.items(1)
    assert toi
    with toi() as values:
        for id, doc in enumerate(values, 1):
            assert doc.id == f"identifier{id}"
            assert doc.Meta.is_embedded == True


def test_entity_items_method_returns_flattened_lists(psc, patch_calling_class_name):
    patch_calling_class_name("Thing")

    class Thing(psc):
        def build_document(self, identifier):
            return [
                Thing.Document(id=f"identifier{identifier}A", thing="thing_value"),
                Thing.Document(id=f"identifier{identifier}B", thing="thing_value"),
            ]

        def build_document_set(self):
            for a in [1, 2, 3]:
                yield self.build_document(a)

    toi = Thing.items([1, 2, 3])
    with toi() as values:
        for id, doc in zip(["1A", "1B", "2A", "2B", "3A", "3B"], values):
            assert doc.id == f"identifier{id}"
            assert doc.Meta.is_embedded == True


def test_entity_items_method_returns_flattened_lists_from_generator(
    psc, patch_calling_class_name
):
    patch_calling_class_name("Thing")

    class Thing(psc):
        def build_document(self, identifier):
            ## SAME AS PREVIOUS TEST, BUT HERE WE'RE yielding from a list
            yield from [
                Thing.Document(id=f"identifier{identifier}A", thing="thing_value"),
                Thing.Document(id=f"identifier{identifier}B", thing="thing_value"),
            ]

        def build_document_set(self):
            for a in [1, 2, 3]:
                yield self.build_document(a)

    toi = Thing.items([1, 2, 3])
    with toi() as values:
        for id, doc in zip(["1A", "1B", "2A", "2B", "3A", "3B"], values):
            assert doc.id == f"identifier{id}"
            assert doc.EmbeddedMeta.is_embedded == True


def test_entity_items_method_raises_error_when_yielding_a_list_by_mistake(
    psc, patch_calling_class_name
):
    patch_calling_class_name("Thing")

    class Thing(psc):
        def build_document(self, identifier):
            # This shouldn't work, because it's an iterable of an iterable —
            # we can't handle everything: someone writing this function should
            # figure that out
            yield [
                Thing.Document(id=f"identifier{identifier}A", thing="thing_value"),
                Thing.Document(id=f"identifier{identifier}B", thing="thing_value"),
            ]

        def build_document_set(self):
            for a in [1, 2, 3]:
                yield self.build_document(a)

    with pytest.raises(PySolaarDocumentError) as e:
        # This error isn't actually raised until we try to unpack the
        # fellow, as only then do we have access to the values and can check...
        # (he says, writing this comment before fixing the code)
        # (he was right)
        toi = Thing.items([1, 2, 3])
        with toi() as values:
            pass


def test_entity_items_method_can_take_meta(thing_object, patch_calling_class_name):
    patch_calling_class_name("Thing")

    class Meta(BaseMeta):
        exclude_fields = ["thing"]

    toi = thing_object.items(1, meta=Meta)
    with toi() as values:
        for doc in values:
            assert doc.EmbeddedMeta.exclude_fields == ["thing"]


def test_entity_items_method_can_take_exclude_field(
    thing_object, patch_calling_class_name
):
    patch_calling_class_name("Thing")

    toi = thing_object.items(1, exclude_fields=["thang"])

    with toi() as values:
        for doc in values:
            assert doc.EmbeddedMeta.exclude_fields == ["thang"]


def test_entity_items_method_can_take_select_field(
    thing_object, patch_calling_class_name
):
    patch_calling_class_name("Thing")
    toi = thing_object.items(1, select_fields=["thang"])

    with toi() as values:
        for doc in values:
            assert doc.EmbeddedMeta.select_fields == ["thang"]


def test_entity_items_method_can_take_select_field_and_override_meta(
    thing_object, patch_calling_class_name
):
    patch_calling_class_name("Thing")

    class Meta(BaseMeta):
        select_fields = ["thing"]

    thing_object.Meta = Meta
    toi = thing_object.items(1)

    with toi() as values:
        for doc in values:
            assert doc.EmbeddedMeta.select_fields == ["thing"]

    toi = thing_object.items(1, select_fields=["thang"])
    with toi() as values:
        for doc in values:
            assert doc.EmbeddedMeta.select_fields == ["thang"]


def test_entity_items_method_can_take_fields_as_json(
    thing_object, patch_calling_class_name
):
    patch_calling_class_name("Thing")

    toi = thing_object.items(1, fields_as_json=["thang"])

    with toi() as values:
        for doc in values:
            assert doc.EmbeddedMeta.fields_as_json == {"thang"}


"""
Not clear why this test
def test_entity_items_method_removes_(thing_object, patch_calling_class_name):
    patch_calling_class_name("Thing")

    class Meta(BaseMeta):
        fields_as_child_docs = ["thing"]
        fields_as_json = ["thang"]

    thing_object.Meta = Meta
    toi = thing_object.items(1)

    with toi() as values:
        d = values[0]
        assert set(d.EmbeddedMeta.fields_as_child_docs) == {"thing"}
        for f in d.EmbeddedMeta.fields_as_child_docs:
            assert f in d.EmbeddedMeta.fields_as_json
"""


def test_complex_setup(psc):
    class Outer(psc):
        class Meta:
            fields_as_child_docs = ["nested"]

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return Outer.Document(
                id=f"outer_id_{identifier}",
                thing="thang",
                nested=Inner.items(1),
            )

    class Inner(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return Inner.Document(
                id=f"inner_id_{identifier}",
                innerthing="innerthang",
            )

    assert Outer.build_document(1).doc_to_solr() == {
        "id": f"Outer{ID_SEPARATOR}outer_id_1",
        "Outer______thing": "thang",
        "_doc": [
            {
                "id": f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}inner_id_1",
                "Outer______nested______innerthing": "innerthang",
                "pysolaar_type_nested": f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}nested",
            }
        ],
        "pysolaar_type": "Outer",
    }


def test_complex_setup_too(psc):
    def something_returning_something_returning_inner():
        return something_returning_inner()

    def something_returning_inner():
        return Inner.items(1)

    class Outer(psc):
        class Meta:
            fields_as_child_docs = ["nested"]

        def build_document_set(self):
            for i in [1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return Outer.Document(
                id=f"outer_id_{identifier}",
                thing="thang",
                nested=something_returning_something_returning_inner(),
            )

    class Inner(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return Inner.Document(
                id=f"inner_id_{identifier}",
                innerthing="innerthang",
            )

    assert Outer.build_document(1).doc_to_solr() == {
        "id": f"Outer{ID_SEPARATOR}outer_id_1",
        "Outer______thing": "thang",
        "_doc": [
            {
                "id": f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}inner_id_1",
                "Outer______nested______innerthing": "innerthang",
                "pysolaar_type_nested": f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}nested",
            }
        ],
        "pysolaar_type": "Outer",
    }


def test_complex_double_nested_as_json(psc):
    def something_returning_something_returning_inner():
        return something_returning_inner()

    def something_returning_inner():
        return Inner.items(1)

    class Outer(psc):
        class Meta:
            fields_as_child_docs = ["nested"]

        def build_document_set(self):
            for i in [1, 2, 3]:
                yield self.build_document(i)

        def build_document(self, identifier):
            return Outer.Document(
                id=f"outer_id_{identifier}",
                thing="thang",
                nested=something_returning_something_returning_inner(),
            )

    class Inner(psc):
        class Meta:
            fields_as_child_docs = ["renested"]

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return Inner.Document(
                id=f"inner_id_{identifier}",
                innerthing="innerthang",
                renested=MoreInner.items(1),
            )

    class MoreInner(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return MoreInner.Document(
                id=f"MoreInner{identifier}", moreinnerthing="moreinnerthang"
            )

    doc = Outer.build_document(1).doc_to_solr()

    assert doc["id"] == f"Outer{ID_SEPARATOR}outer_id_1"
    assert doc["Outer______thing"] == "thang"
    inner = doc["_doc"][0]
    assert inner["id"] == f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}inner_id_1"
    assert inner["Outer______nested______innerthing"] == "innerthang"
    assert (
        inner["pysolaar_type_nested"] == f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}nested"
    )

    assert inner["Outer______nested______renested"] == json.dumps(
        [
            {
                "id": "MoreInner1",
                "moreinnerthing": "moreinnerthang",
            }
        ]
    )


def test_some_mutual_embedding(psc):
    class Thing(psc):
        class Meta:
            fields_as_child_docs = ["embed"]

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return Thing.Document(
                id=f"{identifier}",
                value="something",
                embed=Thing.items([f"{identifier}-{identifier}"]),
            )

    doc = Thing.build_document(1).doc_to_solr()
    assert doc["id"] == f"Thing{ID_SEPARATOR}1"
    assert doc["pysolaar_type"] == "Thing"
    assert doc[f"Thing{KEY_SEPARATOR}value"] == "something"
    assert "embed" not in doc
    assert "_doc" in doc
    # assert len(doc["_doc"]) == 1
    embedded_doc = doc["_doc"][0]
    assert embedded_doc["id"] == f"Thing{ID_SEPARATOR}Thing{ID_SEPARATOR}1-1"
    assert embedded_doc[f"Thing{KEY_SEPARATOR}embed{KEY_SEPARATOR}value"] == "something"
    assert (
        embedded_doc[f"Thing{KEY_SEPARATOR}embed{KEY_SEPARATOR}embed"]
        == '[{"id": "1-1-1-1", "value": "something"}]'
    )


def test_subclassing(psc):
    class Thing(psc):
        def build_document_set():
            pass

        def build_document(self, identifier):
            return Thing.Document(id="thing_document", thing_key="thing_value")

    class Subthing(Thing):
        pass

    class Othersubthing(Thing):
        def build_document(self, identifier):
            return Othersubthing.Document(id="thing_document", thing_key="thing_value")

    assert Thing.build_document(1).doc_to_solr() == {
        "id": f"Thing{ID_SEPARATOR}thing_document",
        "pysolaar_type": "Thing",
        f"Thing{KEY_SEPARATOR}thing_key": "thing_value",
    }

    # Check that our class is there and contented
    assert psc.get_subclass("Subthing")["class"] == Subthing

    assert type(Subthing.build_document(1)).__name__ == "SubthingDocument"

    assert Subthing.build_document(1).doc_to_solr() == {
        "Subthing______thing_key": "thing_value",
        "id": f"Subthing{ID_SEPARATOR}thing_document",
        "pysolaar_type": "Subthing",
    }

    assert Othersubthing.build_document(1).doc_to_solr() == {
        "Othersubthing______thing_key": "thing_value",
        "id": f"Othersubthing{ID_SEPARATOR}thing_document",
        "pysolaar_type": "Othersubthing",
    }


def test_subclassing_with_lists_of_docs(psc):
    class Thing(psc):
        def build_document_set():
            pass

        def build_document(self, identifier):
            return [
                Thing.Document(id="thing_document_a", thing_key="thing_value_a"),
                Thing.Document(id="thing_document_b", thing_key="thing_value_b"),
            ]

    class Subthing(Thing):
        pass

    assert [doc.doc_to_solr() for doc in Thing.build_document(1)] == [
        {
            "id": f"Thing{ID_SEPARATOR}thing_document_a",
            "pysolaar_type": "Thing",
            f"Thing{KEY_SEPARATOR}thing_key": "thing_value_a",
        },
        {
            "id": f"Thing{ID_SEPARATOR}thing_document_b",
            "pysolaar_type": "Thing",
            f"Thing{KEY_SEPARATOR}thing_key": "thing_value_b",
        },
    ]
    assert isinstance(Subthing.build_document(1), list)
    assert [doc.doc_to_solr() for doc in Subthing.build_document(1)] == [
        {
            "id": f"Subthing{ID_SEPARATOR}thing_document_a",
            "pysolaar_type": "Subthing",
            f"Subthing{KEY_SEPARATOR}thing_key": "thing_value_a",
        },
        {
            "id": f"Subthing{ID_SEPARATOR}thing_document_b",
            "pysolaar_type": "Subthing",
            f"Subthing{KEY_SEPARATOR}thing_key": "thing_value_b",
        },
    ]


def test_subclassing_with_generator_returns_right_type(psc):
    """Same as test above, but making sure we get the right type back, this time
    a generator object"""

    class Thing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            yield from [
                Thing.Document(id="thing_document_a", thing_key="thing_value_a"),
                Thing.Document(id="thing_document_b", thing_key="thing_value_b"),
            ]

    class Subthing(Thing):
        pass

    assert isinstance(Thing.build_document(1), (Generator, itertools._tee))
    assert isinstance(Subthing.build_document(1), (Generator, itertools._tee))


def test_return_self_document_is_also_cool(psc):
    class Thing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            # In all the other tests, I've not used *self*...
            return self.Document(id="thing_document", value="stuff")

    doc = Thing.build_document(1).doc_to_solr()
    assert doc == {
        "id": f"Thing{ID_SEPARATOR}thing_document",
        "Thing______value": "stuff",
        "pysolaar_type": "Thing",
    }


def test_DocumentFields():
    assert (
        DocumentFields(
            value=True,
            something=True,
        )
        == {"value": True, "something": True}
    )

    assert (
        ChildDocument(
            value=True,
            something=True,
        )
        == {"value": True, "something": True}
    )

    assert (
        DocumentFields(
            value=True,
            something=ChildDocument(
                other_value=True,
            ),
        )
        == {"value": True, "something": {"other_value": True}}
    )


def test_store_document_fields(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                # crap=True, # We're missing this out deliberately!
                nested=ChildDocument(
                    nested_value=True,
                    back_round_to_thing_again=ChildDocument(
                        value=True,
                        dt=True,
                        something_else=True,
                    ),
                ),
            )

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            # In all the other tests, I've not used *self*...
            return self.Document(
                id="thing_document",
                value="stuff",
                something_else="something else",
                crap="ShouldNotBeHere",
                dt=datetime.datetime(1991, 1, 1),
                nested=NestedThing.items(1),
            )

    class NestedThing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return self.Document(
                id="nested",
                nested_value="nested_stuff",
                back_round_to_thing_again=Thing.items([2, 3]),
            )

    doc = Thing.build_document(1).doc_to_solr()

    assert doc["id"] == f"Thing{ID_SEPARATOR}thing_document"
    assert doc[f"Thing{KEY_SEPARATOR}value"] == "stuff"
    assert f"Thing{KEY_SEPARATOR}crap" not in doc
    assert f"Thing{KEY_SEPARATOR}something_else" not in doc
    subdoc = doc["_doc"][0]
    assert subdoc["id"] == f"Thing{ID_SEPARATOR}NestedThing{ID_SEPARATOR}nested"
    assert (
        subdoc[f"Thing{KEY_SEPARATOR}nested{KEY_SEPARATOR}nested_value"]
        == "nested_stuff"
    )
    assert (
        subdoc[f"Thing{KEY_SEPARATOR}nested{KEY_SEPARATOR}nested_value"]
        == "nested_stuff"
    )

    assert json.loads(
        subdoc[f"Thing{KEY_SEPARATOR}nested{KEY_SEPARATOR}back_round_to_thing_again"]
    ) == [
        {
            "id": "thing_document",
            "value": "stuff",
            "something_else": "something else",
            "dt": "1991-01-01T00:00:00Z",
        },
        {
            "id": "thing_document",
            "value": "stuff",
            "something_else": "something else",
            "dt": "1991-01-01T00:00:00Z",
        },
    ]


def test_store_document_fields_with_JsonChildDocument(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                # crap=True, # We're missing this out deliberately!
                nested=JsonChildDocument(
                    nested_value=True,
                    back_round_to_thing_again=ChildDocument(
                        value=True,
                        dt=True,
                        something_else=True,
                    ),
                ),
            )

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            # In all the other tests, I've not used *self*...
            return self.Document(
                id="thing_document",
                value="stuff",
                something_else="something else",
                crap="ShouldNotBeHere",
                dt=datetime.datetime(1991, 1, 1),
                nested=NestedThing.items(1),
            )

    class NestedThing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return self.Document(
                id="nested",
                nested_value="nested_stuff",
                back_round_to_thing_again=Thing.items([2, 3]),
            )

    doc = Thing.build_document(1).doc_to_solr()

    assert doc["id"] == f"Thing{ID_SEPARATOR}thing_document"
    assert doc[f"Thing{KEY_SEPARATOR}value"] == "stuff"
    assert f"Thing{KEY_SEPARATOR}crap" not in doc
    assert f"Thing{KEY_SEPARATOR}something_else" not in doc
    assert json.loads(doc[f"Thing{KEY_SEPARATOR}nested"]) == [
        {
            "id": "nested",
            "nested_value": "nested_stuff",
            "back_round_to_thing_again": '[{"id": "thing_document", "value": "stuff", "something_else": "something else", "dt": "1991-01-01T00:00:00Z"}, {"id": "thing_document", "value": "stuff", "something_else": "something else", "dt": "1991-01-01T00:00:00Z"}]',
        }
    ]


def test_store_document_fields_with_SplattedChildDocument(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                # crap=True, # We're missing this out deliberately!
                nested=SplattedChildDocument(
                    nested_value=True,
                    back_round_to_thing_again=ChildDocument(
                        value=True,
                        something_else=True,
                    ),
                ),
            )

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            # In all the other tests, I've not used *self*...
            return self.Document(
                id="thing_document",
                value="parentThingValue",
                something_else="parentThingSomethingElse",
                crap="ShouldNotBeHere",
                dt=datetime.datetime(1991, 1, 1),
                nested=NestedThing.items(1),
            )

    class NestedThing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return self.Document(
                id="the_id",
                a_dict={"no": "no", "ning": "nong"},
                nested_value="NestedValue",
                back_round_to_thing_again=Thing.items([2, 3]),
            )

    doc = Thing.build_document(1).doc_to_solr()
    print(doc)

    assert set(doc[f"Thing{KEY_SEPARATOR}nested"]) == set(
        [
            "the_id",
            "NestedValue",
            "thing_document",
            "parentThingValue",
            "parentThingSomethingElse",
        ]
    )


def test_build_document_cache(psc):
    class Thing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return self.Document(id=identifier, thing=f"value-{identifier}")

    # Spy on the build_document function so we can check when it gets called

    # Now call
    doc = Thing.build_document(1)

    assert isinstance(doc, Thing.Document)
    assert len(Thing._DOCUMENT_CACHE) == 1
    assert Thing._DOCUMENT_CACHE[1] is doc

    doc_again = Thing.build_document(1)
    assert doc_again is doc
    assert len(Thing._DOCUMENT_CACHE) == 1

    doc_different = Thing.build_document(2)
    assert doc_different is not doc
    assert len(Thing._DOCUMENT_CACHE) == 2


def test_build_document_cache_with_generator(psc):
    class Thing(psc):
        def build_document_set(self):
            pass

        def build_document(self, identifier):
            yield self.Document(id=identifier, thing=f"value-{identifier}")

    doc = list(Thing.build_document(1))[0]
    assert doc.doc_to_solr() == {
        "id": f"Thing{ID_SEPARATOR}1",
        "Thing______thing": "value-1",
        "pysolaar_type": "Thing",
    }

    assert isinstance(Thing._DOCUMENT_CACHE[1], CachedGenerator)
    assert isinstance(Thing._DOCUMENT_CACHE[1][0], BaseDocument)
    assert Thing._DOCUMENT_CACHE[1][0].doc_to_solr() == {
        "id": f"Thing{ID_SEPARATOR}1",
        "Thing______thing": "value-1",
        "pysolaar_type": "Thing",
    }


def test_build_document_cache_with_nested(psc):
    """Test a whole load of cached things.
    - Nested items are got from cache as well
    - Can select different fields when nested as when just called
        despite cache (who knows if passing Meta objects round is too weird??)
    """

    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                thing=True,
                nested=ChildDocument(
                    nested_thing=True,
                    included_thing=True,
                ),
            )

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return self.Document(
                id=identifier, thing=f"value-{identifier}", nested=Nested.items(2)
            )

    class Nested(psc):
        class Meta:
            select_fields = ["included_thing", "third_thing"]

        def build_document_set(self):
            pass

        def build_document(self, identifier):
            return self.Document(
                id=f"nested={identifier}",
                nested_thing="someshit",
                included_thing="included_thing",
                third_thing="third thing",
            )

    # Now call
    doc = Thing.build_document(1)

    assert isinstance(doc, Thing.Document)
    assert len(Thing._DOCUMENT_CACHE) == 1
    assert Thing._DOCUMENT_CACHE[1] is doc

    assert Nested._DOCUMENT_CACHE == {}

    assert doc.doc_to_solr() == {
        "Thing______thing": "value-1",
        "_doc": [
            {
                "Thing______nested______included_thing": "included_thing",
                "Thing______nested______nested_thing": "someshit",  # This should include this field as it is allowed
                "id": f"Thing{ID_SEPARATOR}Nested{ID_SEPARATOR}nested=2",
                "pysolaar_type_nested": f"Thing{ID_SEPARATOR}Nested{ID_SEPARATOR}nested",
            }
        ],
        "id": f"Thing{ID_SEPARATOR}1",
        "pysolaar_type": "Thing",
    }
    assert Nested._DOCUMENT_CACHE != {}
    assert len(Nested._DOCUMENT_CACHE) == 1

    nested_doc = Nested.build_document(2)
    assert nested_doc is Nested._DOCUMENT_CACHE[2]
    assert nested_doc.doc_to_solr() == {
        "Nested______included_thing": "included_thing",
        "Nested______third_thing": "third thing",
        "id": f"Nested{ID_SEPARATOR}nested=2",
        "pysolaar_type": "Nested",
    }


def test_configure_pysolr(psc):
    psc.configure_pysolr("http://HERE_IS_MY_SOLR_ADDRESS:8080")
    assert isinstance(psc._solr, pysolr.Solr)
    assert psc._solr.url == "http://HERE_IS_MY_SOLR_ADDRESS:8080"


def test_pysolr_always_commit_always_set(psc):
    psc.configure_pysolr("http://HERE_IS_MY_SOLR_ADDRESS:8080")
    assert psc._solr.always_commit == True


def test_pysolr_setup_kwargs_are_passed(psc):
    psc.configure_pysolr(
        "http://HERE_IS_MY_SOLR_ADDRESS:8080",
        always_commit=False,
        verify=True,
        timeout=30,
    )
    assert psc._solr.always_commit == False
    assert psc._solr.verify == True
    assert psc._solr.timeout == 30


def test_psc_fixture_resets_pysolr(psc):
    psc.configure_pysolr("http://HERE_IS_MY_SOLR_ADDRESS:8080")
    assert psc._solr
    assert psc._solr.url == "http://HERE_IS_MY_SOLR_ADDRESS:8080"
    psc._RESET()
    assert psc._solr == None


def test_cannot_call_configure_on_subclasses(psc: PySolaar):
    class Thing(psc):
        def build_document(self, identifier):
            pass

        build_document_set = None

    with pytest.raises(PySolaarConfigurationError):
        Thing.configure_pysolr("something")


def test_get_documents_to_index_iterates_documents(psc):
    class Stuff(psc):
        def build_document_set(self):
            for n in [0, 1, 2, 3, 4]:
                yield self.build_document(n)

        def build_document(self, identifier):
            return self.Document(
                id=f"id-{identifier}",
                value=f"value-{identifier}",
            )

    for n, doc in enumerate(psc._get_documents_to_index()):
        assert isinstance(doc, Stuff.Document)
        assert doc.id == f"id-{n}"
        assert doc.value == f"value-{n}"


def test_missing_build_doc_set_is_ignored_if_set_to_none(psc):
    class Stuff(psc):
        def build_document_set(self):
            for n in [0, 1, 2, 3, 4]:
                yield self.build_document(n)

        def build_document(self, identifier):
            return self.Document(
                id=f"id-{identifier}",
                value=f"value-{identifier}",
            )

    class Other(psc):
        def build_document(self, identifier):
            return self.Document(
                id=f"id-{identifier}",
                value=f"other-{identifier}",
            )

        build_document_set = None

    assert "Stuff" in psc._PySolaar__subclasses
    assert "Other" in psc._PySolaar__subclasses

    for n, doc in enumerate(psc._get_documents_to_index()):
        assert isinstance(doc, Stuff.Document)
        assert doc.id == f"id-{n}"
        assert doc.value == f"value-{n}"


def test_build_documents_as_generators_can_be_used_multiple_times(psc):
    """  """

    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                value=True,
                nested_field=JsonChildDocument(nested_value=True),
                another_nested_field=JsonChildDocument(nested_value=True),
                and_another=JsonChildDocument(nested_value=True),
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id="one",
                value="value",
                nested_field=NestedThing.items(1),
                another_nested_field=NestedThing.items(1),
                and_another=NestedThing.items(1),
            )

    class NestedThing(psc):
        build_document_set = None

        def build_document(self, identifier):
            yield from [
                self.Document(
                    id="nested",
                    nested_value="Nested Value",
                )
            ]

    doc = Thing.build_document("A").doc_to_solr()

    assert (
        doc[f"Thing{KEY_SEPARATOR}nested_field"]
        == '[{"id": "nested", "nested_value": "Nested Value"}]'
    )
    assert (
        doc[f"Thing{KEY_SEPARATOR}another_nested_field"]
        == '[{"id": "nested", "nested_value": "Nested Value"}]'
    )
    assert (
        doc[f"Thing{KEY_SEPARATOR}and_another"]
        == '[{"id": "nested", "nested_value": "Nested Value"}]'
    )


def test_real_example(psc):
    """Test written to locate a bug where dict fields within a store_document_fields
    approach were not being included, due to dynamic setting of meta.selected_fields.j

    Solution (so I know): once we get to calling encode_key_value on individual dict components,
    we set a meta flag so fields aren't gratuitously omitted -- obviously, we w"""

    class FactoidIndex(psc):
        class Meta:
            store_document_fields = DocumentFields(
                createdBy=True,
                createdWhen=True,
                modifiedBy=True,
                modifiedWhen=True,
                statement_refs=JsonChildDocument(
                    id=True,
                ),
                statements=ChildDocument(
                    id=True,
                    name=True,
                    statementType=True,
                    memberOf=True,
                    place=True,
                    relatedToPerson=True,
                    role=True,
                    date=True,
                    statementText=True,
                ),
            )

        build_document_set = None

        def build_document(self, identifier):
            # instance, source = identifier
            instance = identifier
            res = {}

            res["createdBy"] = "Smith"
            res["createdWhen"] = datetime.date(1999, 1, 1)
            res["modifiedBy"] = "Jones"
            res["modifiedWhen"] = datetime.date(1999, 1, 2)

            res["statements"] = StatementIndex.items(["A"])

            return FactoidIndex.Document(id=f"factoid_{'A'}", **res)

    class StatementIndex(psc):
        build_document_set = None

        def build_document(self, identifier):
            res = {
                "@id": "27527_PersonInstitution_96054",
                "name": "",
                "statementType": {
                    "url": "statement_type_url",
                    "label": "statement_type_label",
                },
                "memberOf": {
                    "uri": [
                        "http://d-nb.info/gnd/1700400-7",
                        "/apis/entities/entity/institution/96052/detail",
                    ],
                    "label": "Kaiserlich-Königliches Lyceum (Graz)",
                },
                "place": {},
                "relatesToPerson": {},
                "role": {"url": "NONE", "label": "war Student"},
                "date": {
                    "sortdate": datetime.date(1820, 1, 1),
                    "label": "1820-01-01-1822-01-01",
                },
                "statementText": "Tangl, Karlmann (war Student) Kaiserlich-Königliches Lyceum (Graz)",
            }
            yield self.Document(id=res.pop("@id"), **res)

            res = {
                "@id": "27527_PersonPlace_141779",
                "name": "",
                "statementType": {
                    "url": "statement_type_url",
                    "label": "statement_type_label",
                },
                "memberOf": {},
                "place": {
                    "uri": [
                        "http://sws.geonames.org/2760910/",
                        "/apis/entities/entity/place/21140/detail",
                    ],
                    "label": "Wolfsberg",
                },
                "relatesToPerson": {},
                "role": {"url": "NONE", "label": "ausgebildet in"},
                "date": {"sortdate": None, "label": "None"},
                "statementText": "Tangl, Karlmann (ausgebildet in) Wolfsberg",
            }
            yield self.Document(id=res.pop("@id"), **res)

    doc = FactoidIndex.build_document("A").doc_to_solr()
    assert doc == {
        "id": f"FactoidIndex{ID_SEPARATOR}factoid_A",
        "FactoidIndex______createdBy": "Smith",
        "FactoidIndex______createdWhen": "1999-01-01T00:00:00Z",
        "FactoidIndex______modifiedBy": "Jones",
        "FactoidIndex______modifiedWhen": "1999-01-02T00:00:00Z",
        "_doc": [
            {
                "id": f"FactoidIndex{ID_SEPARATOR}StatementIndex{ID_SEPARATOR}27527_PersonInstitution_96054",
                "FactoidIndex______statements______name": "",
                "FactoidIndex______statements______statementType__url": "statement_type_url",
                "FactoidIndex______statements______statementType__label": "statement_type_label",
                "FactoidIndex______statements______memberOf__uri": [
                    "http://d-nb.info/gnd/1700400-7",
                    "/apis/entities/entity/institution/96052/detail",
                ],
                "FactoidIndex______statements______memberOf__label": "Kaiserlich-Königliches Lyceum (Graz)",
                "FactoidIndex______statements______role__url": "NONE",
                "FactoidIndex______statements______role__label": "war Student",
                "FactoidIndex______statements______date__sortdate": "1820-01-01T00:00:00Z",
                "FactoidIndex______statements______date__label": "1820-01-01-1822-01-01",
                "FactoidIndex______statements______statementText": "Tangl, Karlmann (war Student) Kaiserlich-Königliches Lyceum (Graz)",
                "pysolaar_type_nested": f"FactoidIndex{ID_SEPARATOR}StatementIndex{ID_SEPARATOR}statements",
            },
            {
                "id": f"FactoidIndex{ID_SEPARATOR}StatementIndex{ID_SEPARATOR}27527_PersonPlace_141779",
                "FactoidIndex______statements______name": "",
                "FactoidIndex______statements______statementType__url": "statement_type_url",
                "FactoidIndex______statements______statementType__label": "statement_type_label",
                "FactoidIndex______statements______place__uri": [
                    "http://sws.geonames.org/2760910/",
                    "/apis/entities/entity/place/21140/detail",
                ],
                "FactoidIndex______statements______place__label": "Wolfsberg",
                "FactoidIndex______statements______role__url": "NONE",
                "FactoidIndex______statements______role__label": "ausgebildet in",
                "FactoidIndex______statements______date__sortdate": None,
                "FactoidIndex______statements______date__label": "None",
                "FactoidIndex______statements______statementText": "Tangl, Karlmann (ausgebildet in) Wolfsberg",
                "pysolaar_type_nested": f"FactoidIndex{ID_SEPARATOR}StatementIndex{ID_SEPARATOR}statements",
            },
        ],
        "pysolaar_type": "FactoidIndex",
    }


def test_dict_in_child(psc):
    class Outer(psc):
        class Meta:
            store_document_fields = DocumentFields(
                somekey=True,
                outerdict=True,
                somenested=ChildDocument(
                    somedict=True,
                    someflat=True,
                ),
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id="ID1",
                somekey="somevalue",
                outerdict={"outdict1": "wha", "outdict2": "who"},
                somenested=Inner.items(1),
            )

    class Inner(psc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(
                id="INID1",
                someflat="flat",
                somedict={
                    "inndict1": "what",
                    "inndict2": "who",
                    "indictlist": ["one", "two"],
                },
            )

    doc = Outer.build_document(1).doc_to_solr()
    assert doc == {
        "id": f"Outer{ID_SEPARATOR}ID1",
        "Outer______somekey": "somevalue",
        "Outer______outerdict__outdict1": "wha",
        "Outer______outerdict__outdict2": "who",
        "_doc": [
            {
                "id": f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}INID1",
                "Outer______somenested______someflat": "flat",
                "Outer______somenested______somedict__inndict1": "what",
                "Outer______somenested______somedict__inndict2": "who",
                "Outer______somenested______somedict__indictlist": ["one", "two"],
                "pysolaar_type_nested": f"Outer{ID_SEPARATOR}Inner{ID_SEPARATOR}somenested",
            }
        ],
        "pysolaar_type": "Outer",
    }


def test_pysolaar_class_filter_method_takes_same_args_as_queryset_filter(psc):
    """To kick off search, PySolaar class needs a .filter method,
    which will return the first queryset object. This is done with a proxying
    method, hence these tests..."""

    queryset_filter_params = inspect.signature(PySolaarQuerySetBase.filter).parameters
    pysolaar_filter_params = inspect.signature(psc.filter).parameters

    assert all(
        qfp in pysolaar_filter_params for qfp in queryset_filter_params if qfp != "self"
    )


def test_pysolaar_class_filter_by_distinct_child_method_takes_same_args_as_qs_version(
    psc,
):
    """As above, but for filter_by_distinct_child method"""

    queryset_filter_params = inspect.signature(
        PySolaarQuerySetBase.filter_by_distinct_child
    ).parameters
    pysolaar_filter_params = inspect.signature(psc.filter_by_distinct_child).parameters

    assert all(
        qfp in pysolaar_filter_params for qfp in queryset_filter_params if qfp != "self"
    )


def test_pysolaar_subclass_has_own_queryset(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                something=True,
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id=1, something="what")

    assert issubclass(Thing.QuerySet, PySolaarQuerySetBase)
    assert Thing.QuerySet.__name__ == "ThingQuerySet"
    assert Thing.QuerySet.pysolaar_type == "Thing"
    assert Thing.QuerySet.Meta
    assert Thing.QuerySet.Meta.store_document_fields == DocumentFields(
        something=True,
    )


def test_filter_returns_queryset(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                something=True,
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id=1, something="what")

    f = Thing.filter(something="what")
    assert isinstance(f, Thing.QuerySet)
    assert str(f.q_object) == str(Q(something="what"))


def test_filter_returns_queryset(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                something=True,
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id=1, something="what")

    f = Thing.filter(something="what")
    assert isinstance(f, Thing.QuerySet)
    assert str(f.q_object) == str(Q(something="what"))


def test_filter_by_distinct_child_returns_queryset(psc):
    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                something=True,
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id=1, something="what")

    f = Thing.filter_by_distinct_child(field_name="nesty", something="what")
    assert isinstance(f, Thing.QuerySet)
    assert len(f.child_qs) == 1


def test_psc_method_proxying(psc):
    """ Check that proxying loads of methods works"""

    class Thing(psc):
        class Meta:
            store_document_fields = DocumentFields(
                something=True,
            )

        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id=1, something="what")

    with mock.patch("pysolaar.queryset.PySolaarQuerySetBase.first") as f:
        assert f.call_count == 0

        Thing.first()

        assert f.call_count == 1

    with mock.patch("pysolaar.queryset.PySolaarQuerySetBase.last") as f:
        assert f.call_count == 0

        Thing.last()

        assert f.call_count == 1

    with mock.patch("pysolaar.queryset.PySolaarQuerySetBase._get_results") as f:
        f.return_value = [{"a": "b"}]

        assert len(Thing) == 1
        assert len(Thing.all()) == 1

    with mock.patch("pysolaar.queryset.PySolaarQuerySetBase._get_results") as f:
        f.return_value = [{"a": "b"}]

        for item in Thing:
            assert item == {"a": "b"}

    with mock.patch("pysolaar.queryset.PySolaarQuerySetBase._get_results") as f:
        f.return_value = [{"a": "b"}]

        for item in Thing:
            assert item == {"a": "b"}

        for item in Thing.all():
            assert item == {"a": "b"}