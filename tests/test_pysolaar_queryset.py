import datetime


from pysolaar.utils.encoders_and_decoders import ID_SEPARATOR, KEY_SEPARATOR
import pytest
from unittest import mock

from pysolaar import PySolaar
from pysolaar.queryset import (
    END_MODIFIED_FIELD_REGEX,
    PySolaarQuerySetBase,
    PySolaarQueryError,
    Q,
)
from pysolaar.utils.meta_utils import (
    DocumentFields,
    SingleValue,
    ChildDocument,
    AsDateTime,
    TransformKey,
    TransformValues,
)


@pytest.fixture
def psc():
    """ Our PySolaar class, with subclasses removed between each test """
    PySolaar._RESET()  # Reset subclasses
    yield PySolaar
    PySolaar._RESET()  # Reset subclasses


@pytest.fixture
def FakeQuerySet():
    """The PySolaar.items method uses sys._getframe(n).f_locals["self"].__name__
    in order to get the class in which the call to this method is embedded
    ('calling_class_name'). In order to test, we need to simulate this by
    patching this whole operation.
    """

    with mock.patch("pysolaar.queryset.PySolaarQuerySetBase._get_results") as f:
        f.return_value = [{"a": "b"}]

        class FakeQuerySet(PySolaarQuerySetBase):
            pysolaar_type = "Fake"
            pass

        yield FakeQuerySet


def test_pbq_filter_returns_new_instance(FakeQuerySet):
    p1 = FakeQuerySet()
    assert p1

    p2 = p1.filter(Q(hello="there"))
    assert p1 is not p2

    assert p1.q_object is None
    assert p2.q_object
    assert str(p2.q_object) == str(Q(hello="there"))

    p3 = p2.filter(Q(also="hi"))
    assert str(p2.q_object) == str(Q(hello="there"))
    assert str(p3.q_object) == str(Q(hello="there") & Q(also="hi"))


def test_boost():
    q = Q(something="grand") ^ 2
    assert str(q) == "(something:grand^2)"


def test_pbq_filter_returns_new_instance_with_kwargs(FakeQuerySet):
    p1 = FakeQuerySet()
    assert p1

    p2 = p1.filter(hello="there")
    assert p1 is not p2

    assert p1.q_object is None
    assert p2.q_object
    assert str(p2.q_object) == str(Q(hello="there"))

    p3 = p2.filter(also="hi")
    assert str(p2.q_object) == str(Q(hello="there"))
    assert str(p3.q_object) == str(Q(hello="there") & Q(also="hi"))


def test_complex_query_creation(FakeQuerySet):
    p1 = FakeQuerySet()
    p2 = p1.filter(one="one")
    p3 = p2.filter(Q(two="two") | Q(three="three"))

    assert str(p3.q_object) == str(Q(one="one") & (Q(two="two") | Q(three="three")))


def test_passing_pbq_filter_q_and_kwargs_throws_error(FakeQuerySet):
    p1 = FakeQuerySet()

    with pytest.raises(PySolaarQueryError):
        p1.filter(Q("wha"), also=1)


def test_end_modified_field_regex():
    assert not END_MODIFIED_FIELD_REGEX.match("something")
    assert not END_MODIFIED_FIELD_REGEX.match(f"Thing{KEY_SEPARATOR}something")
    assert END_MODIFIED_FIELD_REGEX.match("field__lt").groups() == ("field", "lt")
    assert END_MODIFIED_FIELD_REGEX.match("field__gt").groups() == ("field", "gt")
    assert END_MODIFIED_FIELD_REGEX.match("field__lte").groups() == ("field", "lte")
    assert END_MODIFIED_FIELD_REGEX.match("field__gte").groups() == ("field", "gte")
    assert END_MODIFIED_FIELD_REGEX.match(
        "SomeEffigingRidiculousShit_____ThatMayBeHere_____field__gte"
    ).groups() == ("SomeEffigingRidiculousShit_____ThatMayBeHere_____field", "gte")


def test_encode_end_modifier_value():
    assert Q._encode_end_modifier_query("field", "lt", 100) == ("field", "[* TO 100}")
    assert Q._encode_end_modifier_query("field", "lte", 100) == ("field", "[* TO 100]")
    assert Q._encode_end_modifier_query("field", "gt", 100) == ("field", "{100 TO *]")
    assert Q._encode_end_modifier_query("field", "gte", 100) == ("field", "[100 TO *]")
    assert Q._encode_end_modifier_query("field", "subfield", "whatever") == (
        "field__subfield",
        "whatever",
    )


def test_q_compile_with_class_name():
    q = Q(thing="thong")
    assert q.compile(class_name="Thing") == "Thing______thing:thong"

    q2 = Q(thing="thong") & Q(ding="dong")
    assert (
        q2.compile(class_name="Thing")
        == "(Thing______thing:thong AND Thing______ding:dong)"
    )

    q3 = Q(thing="thong") & Q(ding__sub="dong")
    assert (
        q3.compile(class_name="Thing")
        == "(Thing______thing:thong AND Thing______ding__sub:dong)"
    )

    q4 = Q(thing="thong") & Q(ding__gt=1)
    assert (
        q4.compile(class_name="Thing")
        == "(Thing______thing:thong AND Thing______ding:{1 TO *])"
    )


def test_q_compiles_id_correctly():
    q = Q(id="whatever")
    assert q.compile(class_name="Thing") == (f"id:Thing{ID_SEPARATOR}whatever")


def test_q_modifies_lt_gt_etc():
    # Normal field
    assert str(Q(hello="yes")) == "hello:yes"
    assert str(Q(age__lt=100)) == "age:[* TO 100}"
    assert (
        str(Q(SomeClass_____some_gumpf__lt=100))
        == "SomeClass_____some_gumpf:[* TO 100}"
    )
    assert (
        str(Q(SomeClass_____some_gumpf__whatever="value"))
        == "SomeClass_____some_gumpf__whatever:value"
    )


def test_q_takes_dates_and_datetimes():
    assert str(Q(d=datetime.datetime(1998, 1, 1))) == 'd:"1998-01-01T00:00:00Z"'
    assert str(Q(d=datetime.date(1998, 1, 1))) == 'd:"1998-01-01T00:00:00Z"'
    assert str(Q(d__lt=datetime.date(1998, 1, 1))) == 'd:[* TO "1998-01-01T00:00:00Z"}'


def test_calling_get_results_deferred_with_iter(FakeQuerySet):
    p = FakeQuerySet()

    assert p._get_results.call_count == 0

    for item in p:
        assert item == {"a": "b"}

    assert p._get_results.call_count == 1


def test_calling_get_results_deferred_with_len(FakeQuerySet):
    p = FakeQuerySet()
    assert p._get_results.call_count == 0

    assert len(p) == 1

    assert p._get_results.call_count == 1

    assert len(p) == 1

    assert p._get_results.call_count == 1


def test_first_method(FakeQuerySet):
    p = FakeQuerySet()
    assert p._get_results.call_count == 0

    assert p.first() == {"a": "b"}
    assert p._get_results.call_args == mock.call(rows=1)

    assert p._get_results.call_count == 1

    assert p.first() == {"a": "b"}

    # First should actually cause results to be
    # got again, as it's not the full set of results!
    # assert p._get_results.call_count == 2
    assert p._get_results.call_args == mock.call(rows=1)


def test_last_method(FakeQuerySet):
    p = FakeQuerySet()
    assert p._get_results.call_count == 0

    assert p.count() == 1

    assert p._get_results.call_args == mock.call(rows=0)


def test_last_method(FakeQuerySet):
    p = FakeQuerySet()
    assert p._get_results.call_count == 0

    assert p.last() == {"a": "b"}

    assert p._get_results.call_count == 1

    assert p.last() == {"a": "b"}

    assert p._get_results.call_count == 1


def test_result_set(FakeQuerySet):
    """tests results_set method; also implicitly tests
    the @_with_result decorator"""
    p = FakeQuerySet()

    assert p._get_results.call_count == 0

    assert p.results_set() == [{"a": "b"}]

    assert p._get_results.call_count == 1


def test_proxy_to_results_with_args(FakeQuerySet):
    p = FakeQuerySet()
    p.first()

    assert p._get_results.call_count == 1
    assert p._get_results.call_args == mock.call(rows=1)


def test_prepare_qs_with_basic_queries(FakeQuerySet):
    p = FakeQuerySet().filter(something="somevalue", otherthing="othervalue")

    assert (
        p._prepare_qs()
        == f"pysolaar_type:Fake AND (Fake{KEY_SEPARATOR}something:somevalue AND Fake{KEY_SEPARATOR}otherthing:othervalue)"
    )

    p = FakeQuerySet().filter(something="balls").filter(other_thing="wank")
    assert (
        p._prepare_qs()
        == f"pysolaar_type:Fake AND (Fake{KEY_SEPARATOR}something:balls AND Fake{KEY_SEPARATOR}other_thing:wank)"
    )


def test_filter_by_distinct_child(FakeQuerySet):

    p = FakeQuerySet().filter_by_distinct_child(
        field_name="nesty", key1="value1", key2="value2"
    )

    assert p.child_qs == [
        (
            "nesty",
            "(Fake______nesty______key1:value1 AND Fake______nesty______key2:value2)",
        )
    ]

    p2 = p.filter_by_distinct_child(field_name="nesty", q=Q(thing="thong"))
    assert p2.child_qs == [
        (
            "nesty",
            "(Fake______nesty______key1:value1 AND Fake______nesty______key2:value2)",
        ),
        (
            "nesty",
            "Fake______nesty______thing:thong",
        ),
    ]


def test_prepare_qs_with_no_filters(FakeQuerySet):
    p = FakeQuerySet()
    assert p._prepare_qs() == "pysolaar_type:Fake"


def test_child_qs_to_fqs(FakeQuerySet):
    p = FakeQuerySet()
    p = p.filter_by_distinct_child(field_name="nesty", thing="thong")
    assert p._child_qs_to_fqs() == [
        (
            '{!parent which="*:* -_nest_path_:* +pysolaar_type:Fake"}'
            "(+_nest_path_:\\/_doc +pysolaar_type_nested:*nesty +pysolaar_type_nested:Fake* "
            "+(Fake______nesty______thing:thong))"
        )
    ]


@pytest.fixture()
def mocked_queryset_and_ps_search(psc):
    with mock.patch("pysolr.Solr.search") as pss:
        psc.configure_pysolr("http://MADEUP")

        class Fake(psc):
            class Meta:
                return_document_fields = DocumentFields(
                    id=TransformKey("@id"),
                    a_string=SingleValue,
                    a_child=ChildDocument(
                        id=TransformKey("@id"),
                        a_child_string=TransformValues(lambda v: v.upper())
                        & SingleValue,
                        a_child_date=AsDateTime,
                    ),
                )

            build_document_set = None

            def build_document(self, identifier):
                pass

        yield Fake.QuerySet, pss
        pss


def test_mocked_queryset_and_ps_search(mocked_queryset_and_ps_search):
    FakeQuerySet, pss = mocked_queryset_and_ps_search

    p = FakeQuerySet()
    p._get_results()

    assert pss.call_args == mock.call(
        "pysolaar_type:Fake",
        fq=[],
        fl="id,pysolaar_type,Fake______a_string,Fake______a_child______a_child_string,Fake______a_child______a_child_date,_doc,pysolaar_type_nested,[child limit=1000000]",
        rows=100000,
        start=0,
        sort=""
    )


def test_paginate(mocked_queryset_and_ps_search):
    FakeQuerySet, pss = mocked_queryset_and_ps_search

    p = FakeQuerySet()

    p = p.paginate(page_size=20, start=100)
    assert type(p) is FakeQuerySet

    p._get_results()
    assert pss.call_args == mock.call(
        "pysolaar_type:Fake",
        fq=[],
        fl="id,pysolaar_type,Fake______a_string,Fake______a_child______a_child_string,Fake______a_child______a_child_date,_doc,pysolaar_type_nested,[child limit=1000000]",
        rows=20,
        start=100,
        sort="",
    )

    q = FakeQuerySet()
    q = q.paginate(page_size=3, page_number=10)

    q._get_results()

    assert pss.call_args == mock.call(
        "pysolaar_type:Fake",
        fq=[],
        fl="id,pysolaar_type,Fake______a_string,Fake______a_child______a_child_string,Fake______a_child______a_child_date,_doc,pysolaar_type_nested,[child limit=1000000]",
        rows=3,
        start=30,
        sort="",
    )


def test_pss_limits_by_return_document_fields(mocked_queryset_and_ps_search):
    FakeQuerySet, pss = mocked_queryset_and_ps_search

    p = FakeQuerySet()

    assert list(p.Meta.fields_and_types.keys()) == [
        "id",
        "a_string",
        "a_child",
    ]
    assert p.default_return_fields == [
        "Fake______a_string",
        "Fake______a_child______a_child_string",
        "Fake______a_child______a_child_date",
    ]
    p._get_results()
    assert pss.call_args == mock.call(
        "pysolaar_type:Fake",
        fq=[],
        fl="id,pysolaar_type,Fake______a_string,Fake______a_child______a_child_string,Fake______a_child______a_child_date,_doc,pysolaar_type_nested,[child limit=1000000]",
        rows=100000,
        start=0,
        sort="",
    )


def test_queryset_initialises_with_correct_default_return_fields(psc):
    class Thing(psc):
        class Meta:
            return_document_fields = DocumentFields(
                thing=True,
                thong=True,
                children=ChildDocument(c_thing=True, c_thong=True),
            )

        build_document_set = None
        build_document = None

    assert Thing.QuerySet().default_return_fields == [
        "Thing______thing",
        "Thing______thong",
        "Thing______children______c_thing",
        "Thing______children______c_thong",
    ]

def test_order_by(psc):
    class Thing(psc):
        def build_document(self, identifier):
            return self.Document(id=identifier, thing=["one", "two"][identifier-1])
        
        def build_document_set(self):
            yield from (self.build_document(n) for n in [1, 2])

    # Test method exists and is proxy'd to PySolaarClass
    assert Thing.QuerySet().order_by
    assert Thing.order_by 

    # Test no arguments raises error
    with pytest.raises(PySolaarQueryError):
        Thing.order_by()

    # Test it returns a QuerySet object
    assert isinstance(Thing.order_by("id asc"), Thing.QuerySet)

    # Test id does not encode field name
    assert Thing.order_by("id asc").kwargs["sort"] == f"id asc"

    # Test other field names are encoded
    assert Thing.order_by("field asc").kwargs["sort"] == f"Thing{KEY_SEPARATOR}field asc"

    # Test not specifying order works
    assert Thing.order_by("field").kwargs["sort"] == f"Thing{KEY_SEPARATOR}field asc"

    # Test multiple orders
    assert Thing.order_by("field", "id desc").kwargs["sort"] == f"Thing{KEY_SEPARATOR}field asc,id desc"

    # Test applying multiple orders
    o = Thing.order_by("field")
    o = o.order_by("field1 asc")
    o = o.order_by("field2 asc")
    assert o.kwargs["sort"] == f"Thing{KEY_SEPARATOR}field2 asc"

    # Test accumulating adds to front
    o = Thing.order_by("field")
    o = o.order_by("field1 asc")
    o = o.order_by("field2 asc", accumulate=True)
    assert o.kwargs["sort"] == f"Thing{KEY_SEPARATOR}field2 asc,Thing{KEY_SEPARATOR}field1 asc"


