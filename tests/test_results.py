import copy
import datetime
import json
import pysolr
import pytest

from pysolaar.utils.meta_utils import (
    AsDateTime,
    AsDict,
    BaseMeta,
    DocumentFields,
    ChildDocument,
    JsonChildDocument,
    JsonToDict,
    SingleValue,
    Transform,
    TransformKey,
    TransformValues,
    _fields_from_meta_return_document_fields,
    _unpack_transforms,
)
from pysolaar.result import (
    PySolaarResultsBase,
    _is_dict_field,
    _response_doc_to_return_values,
    is_transform,
    _unpack_child_docs,
)
from pysolaar.utils.encoders_and_decoders import KEY_SEPARATOR, ID_SEPARATOR
from pysolaar.utils.exceptions import PySolaarConfigurationError

from .test_pysolaar_config import psc

PYSOLR_URL = "http://localhost:8983/solr/test_solr"


@pytest.fixture(scope="module")
def solr():

    client = pysolr.Solr(PYSOLR_URL, always_commit=True)

    client.delete(q="*:*")
    print(client.search("*:*"))
    yield client
    client.delete(q="*:*")  # Reset Solr by trashing everything


@pytest.fixture
def cpsc(psc, solr):
    """ Pre-configured PySolaar class """
    solr.delete(q="*:*")
    psc.configure_pysolr(pysolr_object=solr)
    yield psc


def test_subclass_has_results_class(cpsc):
    cpsc.configure_pysolr("http://MADEUP")

    class Thing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id="1")

    class Other(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id="1")

    assert Thing.Results
    assert Thing.Results.__name__ == "ThingResults"
    assert issubclass(Thing.Results, PySolaarResultsBase)

    assert Thing.QuerySet._results_class is Thing.Results

    tqs = Thing.QuerySet()
    tqs._set_results_class()
    assert Thing.QuerySet._solr.results_cls is Thing.Results
    assert cpsc._solr.results_cls is Thing.Results

    assert Other.QuerySet._results_class is Other.Results
    oqs = Other.QuerySet()
    oqs._set_results_class()
    assert Thing.QuerySet._solr.results_cls is Other.Results
    assert cpsc._solr.results_cls is Other.Results


def test_results_come_in_results_class(cpsc):
    class Thing(cpsc):
        def build_document_set(self):
            yield from [self.build_document(1)]

        def build_document(self, identifier):
            return self.Document(id="1")

    cpsc.update()

    r = Thing.all()
    assert type(r) is Thing.Results


TEST_RESPONSE = {
    "responseHeader": {
        "status": 0,
        "QTime": 0,
        "params": {"q": "pysolaar_type:Thing", "fl": "*,[child]", "wt": "json"},
    },
    "response": {
        "numFound": 1,
        "start": 0,
        "numFoundExact": True,
        "docs": [
            {
                "id": f"Thing{ID_SEPARATOR}1",
                "pysolaar_type": ["Thing"],
                "_version_": 1694931063139205120,
            }
        ],
    },
}


@pytest.fixture
def fake_results_class(cpsc):
    class Fake(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            return self.Document(id="1")

    yield Fake.Results


@pytest.fixture
def build_results(fake_results_class):
    RESPONSE_TEMPLATE = {
        "responseHeader": {
            "status": 0,
            "QTime": 0,
            "params": {"q": "pysolaar_type:Fake", "fl": "*,[child]", "wt": "json"},
        },
        "response": {
            "numFound": 1,
            "start": 0,
            "numFoundExact": True,
            "docs": [
                {
                    "id": f"Fake{ID_SEPARATOR}1",
                    "pysolaar_type": ["Fake"],
                    "_version_": 1694931063139205120,
                }
            ],
        },
    }

    def build(docs):
        response = copy.deepcopy(RESPONSE_TEMPLATE)
        response["docs"] = docs
        return fake_results_class(response)

    return build


def test_build_results_fixture(build_results):
    r = build_results(
        [
            {
                "id": f"Fake{ID_SEPARATOR}1",
                "pysolaar_type": ["Fake"],
                "_version_": 1694931063139205120,
            }
        ]
    )

    assert type(r).__name__ == "FakeResults"


def test_fake_results_class_fixture(fake_results_class):
    assert fake_results_class.pysolaar_type == "Fake"
    assert fake_results_class.__name__ == "FakeResults"


def test_len(fake_results_class):
    f = fake_results_class(TEST_RESPONSE)
    assert len(f) == 1


def test_count(fake_results_class):
    f = fake_results_class(TEST_RESPONSE)
    assert f.count() == 1


def test_return_api(cpsc):
    class Thing(cpsc):
        build_document_set = None

        def build_document(self, identifier):
            pass

        class Meta:
            store_document_fields = DocumentFields(
                a_date=True,
                a_string=True,
                a_list=True,
                not_returned=True,  # Skip returning this by never mentioning it again
                a_child=ChildDocument(
                    a_child_string=True,
                    a_child_date=True,
                ),
            )

            return_document_fields = DocumentFields(
                id=TransformKey(lambda k: "@id"),
                a_date=True,
                a_string=SingleValue,
                a_child=ChildDocument(
                    a_child_string=True,
                    a_child_date=True,
                ),
            )


def test_is_dict_field():
    assert not _is_dict_field("something")
    assert not _is_dict_field("something_separated_by_single_underscores")
    assert not _is_dict_field("something___________separatedbyaloadofundsterscores")
    assert _is_dict_field("dict__field")
    assert _is_dict_field("dict_1__something_1__somethingelse_1")


def test_response_doc_to_return_values_basic():
    class Meta(BaseMeta):
        pass

    doc = {
        "id": f"Fake{ID_SEPARATOR}1",
        "pysolaar_type": ["Fake"],
        "something": "some value",
        "_version_": 1694931063139205120,
    }
    r = _response_doc_to_return_values(doc, Meta)

    assert r == {
        "id": "1",
        "something": "some value",
    }


def test_unpack_transforms():
    class Meta(BaseMeta):
        return_document_fields = DocumentFields(
            id=TransformKey(lambda k: "@id"),
            a_date=True,
            a_string=SingleValue,
            a_child=ChildDocument(
                a_child_string=True,
                a_child_date=True,
            ),
        )

    t = _unpack_transforms(_fields_from_meta_return_document_fields(Meta))
    assert "id" in t
    assert "a_string" in t
    assert len(t) == 2


def test_response_doc_to_return_values():
    class Meta(BaseMeta):
        return_document_fields = DocumentFields(
            id=TransformKey(lambda k: "@id"),
            a_date=AsDateTime,
            a_string=SingleValue,
            a_dict=AsDict(
                dict1=SingleValue,
                dict2=AsDateTime,
            ),
            a_child=ChildDocument(
                a_child_string=True,
                a_child_date=True,
            ),
        )

    doc = {
        "id": f"Fake{ID_SEPARATOR}1",
        "pysolaar_type": ["Fake"],
        f"Fake{KEY_SEPARATOR}a_date": ["1998-01-01T00:00:00Z"],
        f"Fake{KEY_SEPARATOR}a_string": ["some string"],
        f"Fake{KEY_SEPARATOR}should_not_be_here": ["some string"],
        f"Fake{KEY_SEPARATOR}a_dict__dict1": ["something"],
        f"Fake{KEY_SEPARATOR}a_dict__dict2": ["1998-01-01T00:00:00Z"],
        "_version_": 1694931063139205120,
    }
    fields_and_types = _fields_from_meta_return_document_fields(Meta)

    r = _response_doc_to_return_values(
        doc,
        Meta,
        transforms=_unpack_transforms(fields_and_types),
        fields_and_types=fields_and_types,
    )

    assert r["@id"] == "1"
    assert r["a_string"] == "some string"
    assert r["a_date"] == datetime.datetime(1998, 1, 1)
    assert "should_not_be_here" not in r
    print(r)
    assert "a_dict" in r
    assert "dict1" in r["a_dict"]
    assert "dict2" in r["a_dict"]
    assert r["a_dict"]["dict1"] == "something"
    assert r["a_dict"]["dict2"] == datetime.datetime(1998, 1, 1)


def test_response_doc_to_return_values_handles_child():
    class Meta(BaseMeta):
        return_document_fields = DocumentFields(
            id=TransformKey("@id"),
            a_string=SingleValue,
            a_child=ChildDocument(
                id=TransformKey("@id"),
                a_child_string=TransformValues(lambda v: v.upper()) & SingleValue,
                a_child_date=AsDateTime,
            ),
        )

    doc = {
        "id": "NewThing##########1",
        "NewThing______a_string": "NOTHING",
        "_doc": [
            {
                "id": "NewThing##########ChildThing##########C1",
                "NewThing______a_child______a_child_string": ["something"],
                "NewThing______a_child______a_child_date": ["1998-01-01T00:00:00Z"],
                "pysolaar_type_nested": [
                    "NewThing##########ChildThing##########a_child"
                ],
            },
            {
                "id": "NewThing##########ChildThing##########C2",
                "NewThing______a_child______a_child_string": ["other"],
                "NewThing______a_child______a_child_date": ["1998-01-01T00:00:00Z"],
                "pysolaar_type_nested": [
                    "NewThing##########ChildThing##########a_child"
                ],
            },
        ],
        "pysolaar_type": "NewThing",
    }

    fields_and_types = _fields_from_meta_return_document_fields(Meta)

    child_fields_and_types = {
        k: _fields_from_meta_return_document_fields(return_document_fields=v)
        for k, v in fields_and_types.items()
        if isinstance(v, ChildDocument)
    }
    child_transforms = {
        k: _unpack_transforms(v) for k, v in child_fields_and_types.items()
    }

    r = _response_doc_to_return_values(
        doc,
        Meta,
        transforms=_unpack_transforms(fields_and_types),
        fields_and_types=fields_and_types,
        child_fields_and_types=child_fields_and_types,
        child_transforms=child_transforms,
    )

    assert "@id" in r
    assert r["a_string"] == "NOTHING"

    assert "a_child" in r
    assert isinstance(r["a_child"], list)
    assert len(r["a_child"]) == 2

    # Note with this test that we're also throwing a TransformValues
    # and a SingleValue transform onto the result, hence single value, upper case
    assert r["a_child"][0]["a_child_string"] == "SOMETHING"
    assert r["a_child"][0]["a_child_date"] == datetime.datetime(1998, 1, 1)
    assert r["a_child"][0]["@id"] == "C1"

    # Results look like this
    {
        "@id": "1",
        "a_string": "NOTHING",
        "a_child": [
            {
                "@id": "C1",
                "a_child_string": "something",
                "a_child_date": datetime.datetime(1998, 1, 1, 0, 0),
            },
            {
                "@id": "C2",
                "a_child_string": "other",
                "a_child_date": datetime.datetime(1998, 1, 1, 0, 0),
            },
        ],
    }


def test_response_doc_to_return_values_handles_child_when_single_child_returned():
    class Meta(BaseMeta):
        return_document_fields = DocumentFields(
            id=TransformKey("@id"),
            a_string=SingleValue,
            a_child=ChildDocument(
                id=TransformKey("@id"),
                a_child_string=TransformValues(lambda v: v.upper()) & SingleValue,
                a_child_date=AsDateTime,
            ),
        )

    doc = {
        "id": "NewThing##########1",
        "NewThing______a_string": "NOTHING",
        "_doc": {
            "id": "NewThing##########ChildThing##########C1",
            "NewThing______a_child______a_child_string": ["something"],
            "NewThing______a_child______a_child_date": ["1998-01-01T00:00:00Z"],
            "pysolaar_type_nested": ["NewThing##########ChildThing##########a_child"],
        },
        "pysolaar_type": "NewThing",
    }

    fields_and_types = _fields_from_meta_return_document_fields(Meta)

    child_fields_and_types = {
        k: _fields_from_meta_return_document_fields(return_document_fields=v)
        for k, v in fields_and_types.items()
        if isinstance(v, ChildDocument)
    }
    child_transforms = {
        k: _unpack_transforms(v) for k, v in child_fields_and_types.items()
    }

    r = _response_doc_to_return_values(
        doc,
        Meta,
        transforms=_unpack_transforms(fields_and_types),
        fields_and_types=fields_and_types,
        child_fields_and_types=child_fields_and_types,
        child_transforms=child_transforms,
    )

    assert "@id" in r
    assert r["a_string"] == "NOTHING"

    assert "a_child" in r
    assert isinstance(r["a_child"], list)

    # Note with this test that we're also throwing a TransformValues
    # and a SingleValue transform onto the result, hence single value, upper case
    assert r["a_child"][0]["a_child_string"] == "SOMETHING"
    assert r["a_child"][0]["a_child_date"] == datetime.datetime(1998, 1, 1)
    assert r["a_child"][0]["@id"] == "C1"


def test_convert_docs_for_output():
    class Result(PySolaarResultsBase):
        class Meta(BaseMeta):
            # Set pysolaar_type manually here... normally inherted from PySolaar init
            pysolaar_type = "NewThing"
            return_document_fields = DocumentFields(
                id=TransformKey("@id"),
                a_string=SingleValue,
                a_child=ChildDocument(
                    id=TransformKey("@id"),
                    a_child_string=TransformValues(lambda v: v.upper()) & SingleValue,
                    a_child_date=AsDateTime,
                ),
            )

    response = {
        "responseHeader": {
            "status": 0,
            "QTime": 0,
            "params": {"q": "pysolaar_type:Fake", "fl": "*,[child]", "wt": "json"},
        },
        "response": {
            "numFound": 1,
            "start": 0,
            "numFoundExact": True,
            "docs": [
                {
                    "id": "NewThing##########1",
                    "NewThing______a_string": "string1",
                    "_doc": [
                        {
                            "id": "NewThing##########ChildThing##########C1",
                            "NewThing______a_child______a_child_string": ["something"],
                            "NewThing______a_child______a_child_date": [
                                "1998-01-01T00:00:00Z"
                            ],
                            "pysolaar_type_nested": [
                                "NewThing##########ChildThing##########a_child"
                            ],
                        },
                        {
                            "id": "NewThing##########ChildThing##########C2",
                            "NewThing______a_child______a_child_string": ["other"],
                            "NewThing______a_child______a_child_date": [
                                "1998-01-01T00:00:00Z"
                            ],
                            "pysolaar_type_nested": [
                                "NewThing##########ChildThing##########a_child"
                            ],
                        },
                    ],
                    "pysolaar_type": "NewThing",
                },
                {
                    "id": "NewThing##########2",
                    "NewThing______a_string": "string2",
                    "_doc": [
                        {
                            "id": "NewThing##########ChildThing##########C1",
                            "NewThing______a_child______a_child_string": ["something"],
                            "NewThing______a_child______a_child_date": [
                                "1998-01-01T00:00:00Z"
                            ],
                            "pysolaar_type_nested": [
                                "NewThing##########ChildThing##########a_child"
                            ],
                        },
                        {
                            "id": "NewThing##########ChildThing##########C2",
                            "NewThing______a_child______a_child_string": ["other"],
                            "NewThing______a_child______a_child_date": [
                                "1998-01-01T00:00:00Z"
                            ],
                            "pysolaar_type_nested": [
                                "NewThing##########ChildThing##########a_child"
                            ],
                        },
                    ],
                    "pysolaar_type": "NewThing",
                },
            ],
        },
    }

    r = Result(response)

    d_for_o = r._convert_docs_for_output()
    assert d_for_o == [
        {
            "@id": "1",
            "a_string": "string1",
            "a_child": [
                {
                    "@id": "C1",
                    "a_child_string": "SOMETHING",
                    "a_child_date": datetime.datetime(1998, 1, 1, 0, 0),
                },
                {
                    "@id": "C2",
                    "a_child_string": "OTHER",
                    "a_child_date": datetime.datetime(1998, 1, 1, 0, 0),
                },
            ],
        },
        {
            "@id": "2",
            "a_string": "string2",
            "a_child": [
                {
                    "@id": "C1",
                    "a_child_string": "SOMETHING",
                    "a_child_date": datetime.datetime(1998, 1, 1, 0, 0),
                },
                {
                    "@id": "C2",
                    "a_child_string": "OTHER",
                    "a_child_date": datetime.datetime(1998, 1, 1, 0, 0),
                },
            ],
        },
    ]
    assert d_for_o[0]["a_string"] == "string1"
    assert d_for_o


def test_unpack_child_docs_does_not_return_unwanted_fields():
    _doc1 = [
        {
            "id": "NewThing##########ChildThing##########C1",
            "NewThing______a_child______a_child_string": ["something"],
            "NewThing______a_child______a_child_date": ["1998-01-01T00:00:00Z"],
            "pysolaar_type_nested": ["NewThing##########ChildThing##########a_child"],
        },
        {
            "id": "NewThing##########ChildThing##########C2",
            "NewThing______unwanted_child______a_child_string": ["other"],
            "NewThing______unwanted_child______a_child_date": ["1998-01-01T00:00:00Z"],
            "pysolaar_type_nested": [
                "NewThing##########ChildThing##########unwanted_child"
            ],
        },
    ]

    unpacked = _unpack_child_docs(
        _doc1,
        {},
        {"a_child_string": True},
        {"a_child": {"a_child_string": SingleValue}},
        {
            "a_child": ChildDocument(
                a_child_string=SingleValue,
                a_child_date=True,
            )
        },
    )
    assert "a_child" in unpacked
    assert unpacked["a_child"][0]["a_child_string"] == "something"
    assert "unwanted_child" not in unpacked

    _doc2 = [
        {
            "id": "NewThing##########ChildThing##########C1",
            "NewThing______a_child______a_child_string": ["something"],
            "NewThing______a_child______a_child_date": ["1998-01-01T00:00:00Z"],
            "pysolaar_type_nested": ["NewThing##########ChildThing##########a_child"],
        },
    ]

    unpacked2 = _unpack_child_docs(
        _doc2,
        {},
        {"a_child_string": True},
        {"a_child": {"a_child_string": SingleValue}},
        {
            "a_child": ChildDocument(
                a_child_string=SingleValue,
                a_child_date=True,
            )
        },
    )
    assert "a_child" in unpacked2
    assert unpacked["a_child"][0]["a_child_string"] == "something"

    unpacked3 = _unpack_child_docs(
        _doc2,
        {},
        {},
        {},
        {},
    )
    assert "a_child" not in unpacked3


def test_unpack_child_doc_applies_tranforms():
    _doc1 = [
        {
            "id": "NewThing##########ChildThing##########C1",
            "NewThing______a_child______a_child_string": ["something"],
            "NewThing______a_child______a_child_date": ["1998-01-01T00:00:00Z"],
            "pysolaar_type_nested": ["NewThing##########ChildThing##########a_child"],
        },
    ]

    unpacked = _unpack_child_docs(
        _doc1,
        {},  # return_doc
        {"a_child_string": True},  # child_fields_and_types
        {"a_child": {"a_child_string": SingleValue}},  # child_transforms,
        {
            "a_child": ChildDocument(
                a_child_string=SingleValue,
                a_child_date=True,
            )
            & SingleValue
            & TransformKey("CHILDY")
            & Transform(
                lambda k, v: (k, {**v, "a_child_string": v["a_child_string"].upper()})
            ),
        },  # parent_fields_and_types
    )

    assert unpacked["CHILDY"]["a_child_string"] == "SOMETHING"